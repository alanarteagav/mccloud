# McCloud

**Prerrequisitos:**
- Go 1.15
- Python 3.7.3
- PostgreSQL 13.1


**Instrucciones para instalar y ejecutar el cliente y servidor.**
- Ejecutar el comando `. install.sh` en el directorio base (`mccloud`).
- El binario generado (el servidor) puede ejecutarse con el comando `./servidor/bin/cmd [puerto]`.
- El cliente puede ejecutarse con el comando `cliente % python3 -m src.cliente-main` desde el directorio `/cliente/`.

**Documentación del servidor**

https://pkg.go.dev/gitlab.com/alanarteagav/mccloud@v0.0.0-20210203001239-72cb46bca0f1
