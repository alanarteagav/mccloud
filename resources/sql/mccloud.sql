CREATE USER foxmccloud WITH SUPERUSER PASSWORD 'barre11R0ll';

CREATE DATABASE mccloud;

\c mccloud foxmccloud;

CREATE TABLE "usuarios" (
  "id" SERIAL PRIMARY KEY,
  "nombre" text UNIQUE,
  "contraseña" text,
  "fotoperfil" text
);

CREATE TABLE "archivos" (
  "id" SERIAL PRIMARY KEY,
  "usuario" int,
  "nombrearchivo" text,
  "longitud" int,
  "ruta" text
);

INSERT INTO usuarios (nombre, contraseña, fotoperfil) VALUES ('default', 'default', 'default');
INSERT INTO usuarios (nombre, contraseña, fotoperfil) VALUES ('fotosPerfil', 'fotosPerfil', 'fotosPerfil');
