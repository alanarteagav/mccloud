package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"gitlab.com/alanarteagav/mccloud/pkg/servidor"
	"gitlab.com/alanarteagav/mccloud/pkg/archivos"
)

// Indica al usuario cómo debe usarse el programa.
func uso() {
	fmt.Println("Uso: ./servidor-main puerto")
	os.Exit(0)
}

// Crea un socket y un servidor.   El socket escucha las conexiones
// de los clientes en el puerto especificado, y al recibir una, la
// conecta con el servidor.
func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	archivos.IniciarSistema()

	args := os.Args[1:]
	if len(args) != 1 {
		uso()
	}

	instancia := servidor.NuevoServidor()

	escucha, err := net.Listen("tcp", ":" + args[0])
	if err != nil {
		fmt.Printf("Error al intentar escuchar en el puerto %v, servidor terminado.\n", args[0])
		os.Exit(1)
	}
	defer escucha.Close()
	log.Println("Escuchando en el puerto :" + args[0])

	for {
		conn, err := escucha.Accept()
		if err != nil {
			log.Println("Error: ", err)
			continue
		}
		instancia.Conectar(servidor.NuevaConexion(conn))
	}
}
