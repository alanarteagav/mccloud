package usuarios

import (
    "gitlab.com/alanarteagav/mccloud/pkg/datos"
    "gitlab.com/alanarteagav/mccloud/pkg/archivos"
    "gitlab.com/alanarteagav/mccloud/pkg/errores"
    "fmt"
)

// Ruta para archivo de foto de perfil por omisión.
const (
    FOTOPERFIL = archivos.FOTOSPERFIL + "/" + "default.png"
)

// Estructura Usuario.
// Contiene una representación de sus datos y una conexión a la base de datos.
type Usuario struct {
    datos    *datos.DatoUsuario
    conexion *datos.ConexionPostgres
}

// Crea un usuario.
// Recibe un nombre de usuario, contraseña y conexión a base de datos.
// Devuelve una estructura Usuario.
func CrearUsuario(nombre string, contraseña string,
                  conexion *datos.ConexionPostgres) (*Usuario, error) {
    usuario := new(Usuario)
    datos := datos.NuevoDatoUsuario(nombre, Encriptar(contraseña), FOTOPERFIL)
    usuario.conexion = conexion
    id, err := conexion.InsertarUsuario(datos)
    if err != nil {
        return nil, err
    }
    datos.SetId(id)
    usuario.datos = datos
    return usuario, nil
}

// Autentica un usuario.
// Recibe un nombre de usuario, contraseña y conexión a base de datos.
// Devuelve una estructura Usuario y un código de error
func Autenticar(nombre string, contraseña string,
                conexion *datos.ConexionPostgres) (*Usuario, errorUsuario) {
    hash, err := conexion.Contraseña(nombre)
    if err != nil {
        switch err {
        case errores.ErrorNoEncontrado:
            return nil, AUTH
        default:
            fmt.Println("Error")
        }
    }
    if Verificar(hash, contraseña) {
        datos, err := conexion.ObtenerUsuario(nombre)
        if err != nil {
            switch err {
            case errores.ErrorNoEncontrado:
                return nil, AUTH
            default:
                fmt.Println("Error")
            }
        }
        usuario := new(Usuario)
        usuario.datos = datos
        usuario.conexion = conexion
        return usuario, NOERROR
    }
    return nil, AUTH
}

// Obtiene la estructura DatoUsuario de un Usuario
// y la devuelve.
func (usuario *Usuario) Datos() *datos.DatoUsuario {
  return usuario.datos
}

// Obtiene el id de una estructura Usuario.
// Devuelve un id entero.
func (usuario *Usuario) Id() int {
    return usuario.datos.Id()
}

// Obtiene el nombre de una estructura Usuario.
// Devuelve una cadena con el nombre.
func (usuario *Usuario) Nombre() string {
    return usuario.datos.Nombre()
}

// Crea un archivo para el usuario.
// Recibe el nombre del archivo, su longitud en bytes y un apuntador a sus bytes.
// Devuelve una estructura Archivo.
func (usuario *Usuario) CrearArchivo(nombreArchivo string, longitud int,
                                     bytes *[]byte) (*archivos.Archivo, error) {
    archivo := new(archivos.Archivo)
    directorioArchivo := usuario.Nombre() + "/"
    datos := datos.NuevoDatoArchivo(usuario.Id(), nombreArchivo, longitud,
                                    directorioArchivo)
    archivo.SetBytes(bytes)
    archivo.SetDatos(datos)
    id, err := usuario.conexion.InsertarArchivo(datos)
    if err != nil {
        return nil, err
    }
    datos.SetId(id)
    err = archivos.CrearDirectorio(directorioArchivo)
    if err != nil {
        return nil, err
    }
    err = archivos.CrearArchivo(directorioArchivo + nombreArchivo, bytes)
    if err != nil {
        return nil, err
    }
    return archivo, nil
}

// Crea un archivo para el usuario.
// Recibe el nombre del archivo, su longitud en bytes y un apuntador a sus bytes.
// Devuelve una estructura Archivo.
func (usuario *Usuario) LeerArchivo(id int) (*archivos.Archivo, error) {
    archivo := new(archivos.Archivo)
    datos, err := usuario.conexion.ObtenerArchivo(id, usuario.Id())
    if err != nil {
        fmt.Println(datos)
        fmt.Println("h")
        fmt.Println(err)
        return nil, err
    }
    directorioArchivo := usuario.Nombre() + "/"
    bytes, err := archivos.LeerArchivo(directorioArchivo + datos.NombreArchivo())
    if err != nil {
        return nil, err
    }
    archivo.SetBytes(bytes)
    archivo.SetDatos(datos)
    return archivo, nil
}

// Elimina un archivo del usuario.
// Recibe el id del archivo.
func (usuario *Usuario) EliminarArchivo(id int) (error) {
    datos, err := usuario.conexion.ObtenerArchivo(id, usuario.Id())
    if err != nil {
        return err
    }
    err = archivos.EliminarArchivo(datos.Ruta() + datos.NombreArchivo())
    if err != nil {
        return err
    }
    err = usuario.conexion.EliminarArchivo(usuario.datos, id)
    if err != nil {
        return err
    }
    return nil
}

// Obtiene la foto de perfil del usuario.
// Devuelve un apuntador a un arreglo de bytes.
func (usuario *Usuario) ObtenerFotoPerfil() (*[]byte, error) {
    ruta, err := usuario.conexion.ObtenerFotoPerfil(usuario.datos)
    if err != nil {
        return nil, err
    }
    bytesFoto, err := archivos.LeerArchivo(ruta)
    if err != nil {
        return nil, err
    }
    return bytesFoto, nil
}

// Actualiza la foto de perfil del usuario.
// Recibe un apuntador a un arreglo de bytes y una cadena con la extensión de
// la foto.
func (usuario *Usuario) ActualizarFotoPerfil(bytes *[]byte,
                                             extension string) error {
    ruta := archivos.FOTOSPERFIL + "/" + usuario.Nombre() + extension
    err := usuario.conexion.ActualizarFotoPerfil(usuario.datos, ruta)
    if err != nil {
        return err
    }
    err = archivos.CrearArchivo(ruta, bytes)
    if err != nil {
        return err
    }
    return nil
}

// Elimina un usuario del sistema.
func (usuario *Usuario) Eliminar() error {
    err := archivos.EliminarDirectorio(usuario.Nombre())
    if err != nil {
        return err
    }
    err = usuario.conexion.EliminarUsuario(usuario.datos)
    if err != nil {
        return err
    }
    return nil
}
