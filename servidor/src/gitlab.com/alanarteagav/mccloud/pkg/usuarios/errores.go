package usuarios

type errorUsuario int

// Códigos de error para usuarios. NOERROR / 0 indica que no hay error.
const (
    NOERROR errorUsuario = iota
    AUTH    errorUsuario = 800 + iota
)
