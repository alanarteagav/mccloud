package usuarios

import (
    "golang.org/x/crypto/bcrypt"
    "fmt"
    "log"
)

// Encripta una cadena.
// Recibe una cadena a encriptar.
// Devuelve el hash asociado.
func Encriptar(contraseña string) string {
    bytes := []byte(contraseña)
    hash, err := bcrypt.GenerateFromPassword(bytes, bcrypt.MinCost)
    if err != nil {
        fmt.Println("Error de encriptación.")
        log.Println(err)
    }
    return string(hash)
}

// Verifica si un hash corresponde a la encriptación de una cadena.
// Recibe un hash y una cadena.
// Devuelve true si el hash corresponde a la cadena false en otro caso.
func Verificar(hash string, contraseña string) bool {
    hashBytes := []byte(hash)
    contraseñaBytes := []byte(contraseña)
    err := bcrypt.CompareHashAndPassword(hashBytes, contraseñaBytes)
    if err != nil {
        return false
        fmt.Println("YO")
    }
    return true
}
