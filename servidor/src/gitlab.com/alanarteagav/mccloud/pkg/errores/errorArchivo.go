package errores

import (
    "errors"
)

var (
    ErrorLecturaArchivo = errors.New("Error: Error al leer archivo.")
    ErrorCreacionArchivo = errors.New("Error: Error al crear archivo.")
    ErrorEscrituraArchivo = errors.New("Error: Error al escribir archivo.")
    ErrorCreacionSistema = errors.New("Error: Error al sistema de archivos")
    ErrorCreacionDirectorio = errors.New("Error: Error al crear directorio")
    ErrorEliminacionDirectorio = errors.New("Error: Error al eliminar directorio")
    ErrorEliminacionArchivoSistema = errors.New("Error: Error al eliminar archivo del sistema")
)
