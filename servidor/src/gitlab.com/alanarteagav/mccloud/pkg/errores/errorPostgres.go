package errores

import (
    "errors"
)

var (
    ErrorLecturaPostgres = errors.New("Error: Error al leer de la base de datos.")
    ErrorUsuarioDuplicado = errors.New("Error: Entrada de usuario duplicada.")
    ErrorArchivoDuplicado = errors.New("Error: Entrada de archivo duplicada.")
    ErrorNoEncontrado = errors.New("Error: Entrada inexistente.")
    ErrorIdUsuario = errors.New("Error: Id de usuario inválido.")
    ErrorDesconocido = errors.New("Error: Fallo Base de Datos.")
    ErrorActualizacion = errors.New("Error: Error al actualizar entrada.")
    ErrorEliminacionArchivos = errors.New("Error: Error al eliminar archivos de la BDD.")
    ErrorEliminacionArchivo = errors.New("Error: Error al eliminar archivo de la BDD.")
    ErrorEliminacionUsuario = errors.New("Error: Error al usuario de la BDD.")
)
