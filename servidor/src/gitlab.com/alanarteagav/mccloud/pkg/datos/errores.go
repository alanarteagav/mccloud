package datos

type errorPostgres int

// Códigos de error para base de datos. NOERROR / 0 indica que no hay error.
const (
    NOERROR  errorPostgres = iota
    UNIQUE   errorPostgres = 700 + iota
    NOTFOUND errorPostgres = 700 + iota
    UNKNOWN  errorPostgres = 700 + iota
)
