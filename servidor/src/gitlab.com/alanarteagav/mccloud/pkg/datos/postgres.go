package datos;

import (
  "database/sql"
  "fmt"
  "github.com/lib/pq"
  "gitlab.com/alanarteagav/mccloud/pkg/errores"
  "log"
)

// Datos para conexión a base de datos.
const (
  host     = "localhost"
  port     = 5432
  user     = "foxmccloud"
  password = "barre11R0ll"
  dbname   = "mccloud"
)

// Estructura ConexionPostgres para conexion a la base de datos.
type ConexionPostgres struct {
    conexion *sql.DB
}

// Instancia una estructura ConexionPostgres.
func NuevaConexion() *ConexionPostgres {
    postgres := new(ConexionPostgres)
    psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
        "password=%s dbname=%s sslmode=disable",
        host, port, user, password, dbname)
    conexion, err := sql.Open("postgres", psqlInfo)
    if err != nil {
      panic(err)
    }
    //defer conexion.Close()

    err = conexion.Ping()
    if err != nil {
      panic(err)
    }
    log.Println("Conexión exitosa a la base de datos.")
    postgres.conexion = conexion
    return postgres
}

// Inserta los datos de un usuario a la base de datos
// Recibe una estructura DatoUsuario.
// Devuelve el id del usuario en la base de datos.
func (postgres *ConexionPostgres) InsertarUsuario(usuario *DatoUsuario) (int, error){
    sqlStatement := `
    INSERT INTO usuarios (nombre, contraseña, fotoperfil)
    VALUES ($1, $2, $3)
    RETURNING id`
    id := 0
    err := postgres.conexion.QueryRow(sqlStatement,
                                      usuario.nombre,
                                      usuario.contraseña,
                                      usuario.fotoPerfil).Scan(&id)
    if err != nil {
        if err, ok := err.(*pq.Error); ok {
            switch err.Code.Name() {
            case "unique_violation":
                return 0, errores.ErrorUsuarioDuplicado
            default:
                return 0, errores.ErrorDesconocido
            }
        }
    }
    usuario.SetId(id)
    return id, nil
}

// Inserta los datos de un archivo a la base de datos
// Recibe una estructura DatoArchivo.
// Devuelve el id del archivo en la base de datos.
func (postgres *ConexionPostgres) InsertarArchivo(archivo *DatoArchivo) (int, error){
    existeArchivo, err := postgres.existeArchivo(archivo.usuario, archivo.nombreArchivo, archivo.ruta)
    if err != nil {
        return 0, err
    }
    if existeArchivo {
        return 0, errores.ErrorArchivoDuplicado
    }
    sqlStatement := `
    INSERT INTO archivos (usuario, nombrearchivo, longitud, ruta)
    VALUES ($1, $2, $3, $4)
    RETURNING id`
    id := 0
    err = postgres.conexion.QueryRow(sqlStatement,
                                      archivo.usuario,
                                      archivo.nombreArchivo,
                                      archivo.longitud,
                                      archivo.ruta).Scan(&id)
    if err != nil {
        if err, ok := err.(*pq.Error); ok {
            switch err.Code.Name() {
            case "unique_violation":
                return 0, errores.ErrorArchivoDuplicado
            default:
                return 0, errores.ErrorDesconocido
            }
        }
    }
    archivo.SetId(id)
    return id, nil
}

// Devuelve los datos de los archivos de cierto usuario.
// Recibe una estructura DatoUsuario.
// Devuelve un slice de estructuras DatoArchivo.
func (postgres *ConexionPostgres) ListarArchivos(usuario *DatoUsuario) ([]*DatoArchivo, error){
    sqlStatement := `SELECT * FROM archivos WHERE usuario=$1;`

    var archivos []*DatoArchivo

    if usuario.id == 0 {
        return nil, errores.ErrorIdUsuario
    }

    filas, err := postgres.conexion.Query(sqlStatement, usuario.id)
    if err != nil {
        return nil, errores.ErrorLecturaPostgres
    }
    defer filas.Close()
    for filas.Next() {

        var id int
        var usuario int
        var nombrearchivo string
        var longitud int
        var ruta string

        err = filas.Scan(&id, &usuario, &nombrearchivo, &longitud, &ruta)
        if err != nil {
            return nil, errores.ErrorLecturaPostgres
        }
        archivo := NuevoDatoArchivo(usuario, nombrearchivo, longitud, ruta)
        archivo.SetId(id)
        archivos = append(archivos, archivo)
    }
    err = filas.Err()
    if err != nil {
        return nil, errores.ErrorLecturaPostgres
    }
    return archivos, nil
}

// Verifica si un archivo existe para cierto usuario.
// Recibe un id de usuario, un nombre de archivo y una ruta de archivo.
// Devuelve true si existe, false en otro caso.
func (postgres *ConexionPostgres) existeArchivo(usuario int,
                                                nombrearchivo string,
                                                ruta string) (bool, error) {
    sqlStatement := `SELECT * FROM archivos
                     WHERE usuario=$1 and nombrearchivo=$2 and ruta=$3;`
    var archivos []*DatoArchivo
    if usuario == 0 {
        return false, nil
    }
    filas, err := postgres.conexion.Query(sqlStatement,
                                          usuario,
                                          nombrearchivo,
                                          ruta)
    if err != nil {
        return false, errores.ErrorLecturaArchivo
    }
    defer filas.Close()
    for filas.Next() {

        var id int
        var usuario int
        var nombrearchivo string
        var longitud int
        var ruta string

        err = filas.Scan(&id, &usuario, &nombrearchivo, &longitud, &ruta)
        if err != nil {
            return false, errores.ErrorLecturaArchivo
        }
        archivo := NuevoDatoArchivo(usuario, nombrearchivo, longitud, ruta)
        archivo.SetId(id)
        archivos = append(archivos, archivo)
    }
    err = filas.Err()
    if err != nil {
        return false, errores.ErrorLecturaArchivo
    }
    return len(archivos) >= 1, nil
}

// Obtiene los datos de un usuario, dado su nombre.
// Recibe nombre de usuario.
// Devuelve una estructura DatoUsuario y un código de error.
func (postgres *ConexionPostgres) ObtenerUsuario (nombre string) (*DatoUsuario, error) {
    sqlStatement := `SELECT * FROM usuarios
                     WHERE nombre=$1;`
    var id int
    var contraseña string
    var fotoPerfil string

    row := postgres.conexion.QueryRow(sqlStatement, nombre)
    err := row.Scan(&id, &nombre, &contraseña, &fotoPerfil)
    if err != nil {
        switch err {
        case sql.ErrNoRows:
            return nil, errores.ErrorNoEncontrado
        default:
            panic(err)
            return nil, errores.ErrorDesconocido
        }
    }
    usuario := NuevoDatoUsuario(nombre, contraseña, fotoPerfil)
    usuario.SetId(id)
    return usuario, nil
}

// Obtiene los datos de un usuario, dado su nombre.
// Recibe nombre de usuario.
// Devuelve una estructura DatoUsuario y un código de error.
func (postgres *ConexionPostgres) ObtenerArchivo (id int,
                                                  usuario int) (*DatoArchivo, error) {
    sqlStatement := `SELECT * FROM archivos
                     WHERE id=$1 and usuario=$2;`
    var nombreArchivo string
    var longitud int
    var ruta string

    row := postgres.conexion.QueryRow(sqlStatement, id, usuario)
    err := row.Scan(&id, &usuario, &nombreArchivo, &longitud, &ruta)
    if err != nil {
        switch err {
        case sql.ErrNoRows:
            return nil, errores.ErrorNoEncontrado
        default:
            return nil, errores.ErrorDesconocido
        }
    }
    archivo := NuevoDatoArchivo(usuario, nombreArchivo, longitud, ruta)
    archivo.SetId(id)
    return archivo, nil
}

// Obtiene la contraseña de un usuario, dado su nombre.
// Recibe nombre de usuario.
// Devuelve una cadena con la contraseña y un código de error.
func (postgres *ConexionPostgres) Contraseña (nombre string) (string, error) {
    sqlStatement := `SELECT contraseña FROM usuarios
                     WHERE nombre=$1;`
    var contraseña string

    row := postgres.conexion.QueryRow(sqlStatement, nombre)
    err := row.Scan(&contraseña)
    if err != nil {
        switch err {
        case sql.ErrNoRows:
            return "", errores.ErrorNoEncontrado
        default:
            return "", errores.ErrorDesconocido
        }
    }
    return contraseña, nil
}

// Actualiza la ruta de la foto de perfil de un usuario.
// Recibe una estructura DatoUsuario y una ruta.
func (postgres *ConexionPostgres) ActualizarFotoPerfil (usuario *DatoUsuario,
                                                        ruta string) error {
    sqlStatement := `
    UPDATE usuarios
    SET fotoperfil = $2
    WHERE id = $1;`
    _, err := postgres.conexion.Exec(sqlStatement, usuario.id, ruta)
    if err != nil {
        return errores.ErrorActualizacion
    }
    return nil
}

// Obtiene la ruta de la foto de perfil de un usuario.
// Recibe una estructura DatoUsuario.
// Devuelve una cadena con la ruta de la foto de perfil.
func (postgres *ConexionPostgres) ObtenerFotoPerfil (usuario *DatoUsuario) (string, error) {
    sqlStatement := `SELECT fotoperfil FROM usuarios
                     WHERE id=$1;`
    var fotoPerfil string

    row := postgres.conexion.QueryRow(sqlStatement, usuario.id)
    err := row.Scan(&fotoPerfil)
    if err != nil {
        switch err {
        case sql.ErrNoRows:
            return "", errores.ErrorNoEncontrado
        default:
            panic(err)
            return "", errores.ErrorDesconocido
        }
    }
    return fotoPerfil, nil
}

// Elimina los arhivos de un usuario de la base de datos.
// Recibe una estructura DatoUsuario.
func (postgres *ConexionPostgres) eliminarArchivos(usuario *DatoUsuario) error {
    sqlStatement := `
    DELETE FROM archivos
    WHERE usuario = $1;`
    _, err := postgres.conexion.Exec(sqlStatement, usuario.id)
    if err != nil {
        return errores.ErrorEliminacionArchivos
    }
    return nil
}

// Elimina un arhivo de un usuario de la base de datos.
// Recibe una estructura DatoUsuario y el id de un archivo.
func (postgres *ConexionPostgres) EliminarArchivo(usuario *DatoUsuario,
                                                  id int) error {
    sqlStatement := `
    DELETE FROM archivos
    WHERE usuario = $1 and id = $2;`
    _, err := postgres.conexion.Exec(sqlStatement, usuario.id, id)
    if err != nil {
        return errores.ErrorEliminacionArchivo
    }
    return nil
}

// Elimina un usuario de la base de datos.
// Recibe una estructura DatoUsuario.
func (postgres *ConexionPostgres) EliminarUsuario(usuario *DatoUsuario) error {
    err := postgres.eliminarArchivos(usuario)
    if err != nil {
        return err
    }
    sqlStatement := `
    DELETE FROM usuarios
    WHERE id = $1;`
    _, err = postgres.conexion.Exec(sqlStatement, usuario.id)
    if err != nil {
        return errores.ErrorEliminacionUsuario
    }
    return nil
}
