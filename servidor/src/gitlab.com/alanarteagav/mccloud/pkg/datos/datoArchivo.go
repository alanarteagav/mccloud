package datos;

// Estructura DatoArchivo.
// Representación de los datos de un archivo que se almacenan en la base de
// datos.
type DatoArchivo struct {
    id int
    usuario int
    nombreArchivo string
    longitud int
    ruta string
}

// Instancia una estructura DatoArchivo.
// Recibe id de usuario, nombre de archivo, longitud en bytes del archivo
// y ruta asociada en el sistema de archivos.
func NuevoDatoArchivo(usuario int, nombreArchivo string, longitud int,
                  ruta string) *DatoArchivo {
    archivo := new(DatoArchivo)
    archivo.usuario = usuario
    archivo.nombreArchivo = nombreArchivo
    archivo.longitud = longitud
    archivo.ruta = ruta
    return archivo
}

// Obtiene el id de una estructura DatoArchivo.
// Devuelve un id entero.
func (archivo *DatoArchivo) Id() int {
    return archivo.id
}

// Establece el id de una estructura DatoArchivo.
// Recibe un id entero.
func (archivo *DatoArchivo) SetId(id int) {
    archivo.id = id
}


// Obtiene el nombre de un DatoArchivo y lo
// devuelve como una cadena.
func (archivo *DatoArchivo) NombreArchivo() string {
  return archivo.nombreArchivo
}

// Obtiene la longitud de un DatoArchivo y la
// devuelve como un entero.
func (archivo *DatoArchivo) Longitud() int {
  return archivo.longitud
}

// Obtiene la ruta de un DatoArchivo y la devuelve como una cadena.
func (archivo *DatoArchivo) Ruta() string {
    return archivo.ruta
}
