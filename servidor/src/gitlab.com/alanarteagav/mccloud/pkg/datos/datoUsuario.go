package datos;

// Estructura DatoUsuario.
// Representación de los datos de un usuario que se almacenan en la base de
// datos.
type DatoUsuario struct {
    id int
    nombre string
    contraseña string
    fotoPerfil string
}

// Instancia una estructura DatoUsuario.
// Recibe nombre de usuario, contraseña, y ruta de foto de perfil.
func NuevoDatoUsuario(nombre string, contraseña string, fotoPerfil string) *DatoUsuario {
    usuario := new(DatoUsuario)
    usuario.nombre = nombre
    usuario.contraseña = contraseña
    usuario.fotoPerfil = fotoPerfil
    return usuario
}

// Obtiene el id de una estructura DatoUsuario.
// Devuelve un id entero.
func (usuario *DatoUsuario) Id() int {
    return usuario.id
}


// Establece el id de una estructura DatoUsuario.
// Recibe un id entero.
func (usuario *DatoUsuario) SetId(id int) {
    usuario.id = id
}

// Obtiene el nombre de una estructura DatoUsuario.
// Devuelve una cadena con el nombre.
func (usuario *DatoUsuario) Nombre() string {
    return usuario.nombre
}
