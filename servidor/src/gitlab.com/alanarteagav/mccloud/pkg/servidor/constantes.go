package servidor

// Constantes para utilizar en el servidor.
const (
	CONN_TIPO = "tcp"

	TIEMPO_TIMEOUT = 300

	EDO_INICIO = 0
	EDO_LOGGED = 2
	EDO_ACCARC = 3
	EDO_ESPVER = 6
	EDO_GRANDE = 666
	EDO_GRANDF = 777

	ERROR_PREFIJO       = "Error: "
	ERROR_NO_RECONOCIDO = ERROR_PREFIJO + "El cliente %v envió un comando no reconocido: %v.\n"
	ERROR_NO_PERMITIDO  = ERROR_PREFIJO + "El cliente %v intentó realizar una acción no permitida en este estado.\n"
	ERROR_INTERNO       = ERROR_PREFIJO + "Ocurrió un error interno en el servidor: %v\n"
	ERROR_REGISTRO      = ERROR_PREFIJO + "El registro del cliente %v falló.\n"
	ERROR_AUTENTICACION = ERROR_PREFIJO + "La autenticación para el cliente %v falló.\n"
	ERROR_ESPACIO       = ERROR_PREFIJO + "El usuario %v solicitó subir un archivo de %v bytes, pero no tiene espacio suficiente.\n"
	ERROR_RECEPCION     = ERROR_PREFIJO + "Falló la recepción de un archivo proveniente del usuario %v.\n"
	ERROR_ENVIO         = ERROR_PREFIJO + "No fue posible obtener el archivo %v solicitado por %v.\n"
	ERROR_ELIMINAR_U    = ERROR_PREFIJO + "No fue posible eliminar al usuario %v.\n"
	ERROR_ELIMINAR_A    = ERROR_PREFIJO + "El usuario %v solicitó eliminar el archivo con ID %v, pero no fue posible.\n"
	ERROR_FOTO          = ERROR_PREFIJO + "No fue posible cambiar la foto de perfil del usuario %v.\n"

	EXITO_PREFIJO       = "Éxito: "
	EXITO_ABORTAR       = EXITO_PREFIJO + "El cliente %v regresó al menú principal.\n"
	EXITO_REGISTRO      = EXITO_PREFIJO + "El cliente %v se registró exitosamente con nombre %s.\n"
	EXITO_AUTENTICACION = EXITO_PREFIJO + "El cliente %v se autenticó como %s.\n"
	EXITO_ESPACIO       = EXITO_PREFIJO + "El usuario %v solicitó subir un archivo de %v bytes y cuenta con espacio suficiente.\n"
	EXITO_DESCONECTA    = EXITO_PREFIJO + "El cliente %v terminó sesión.\n"
	EXITO_RECEPCION     = EXITO_PREFIJO + "Se recibió un archivo exitosamente del usuario %v.\n"
	EXITO_ENVIO         = EXITO_PREFIJO + "Se envió el archivo %v a %v.\n"
	EXITO_ELIMINAR_U    = ERROR_PREFIJO + "Se eliminó al usuario %v permanentemente.\n"
	EXITO_ELIMINAR_A    = ERROR_PREFIJO + "El usuario %v eliminó el archivo con ID %v.\n"
	EXITO_FOTO          = EXITO_PREFIJO + "Se cambió la foto de perfil del usuario %v.\n"

	EMPIEZA_RECEPCION = "El usuario %v comenzó a recibir un archivo en partes.\n"
	EMPIEZA_FOTO      = "El usuario %v comenzó a recibir una foto en partes.\n"

	HAY_ARCHIVOS    = "Se envió una lista no vacía de archivos al usuario %v.\n"
	NO_HAY_ARCHIVOS = "Se envió una lista vacía de archivos al usuario %v.\n"
)
