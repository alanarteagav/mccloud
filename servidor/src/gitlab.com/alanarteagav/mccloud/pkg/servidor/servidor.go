package servidor

import (
	"log"
	"time"
)

// Servidor administra a los clientes conectados y
// responde sus peticiones.   Tiene tres canales,
// entrante, donde recibe mensajes de los clientes
// conectados; unirse, donde recibe solicitudes de
// conexiones nuevas, y salir, donde recibe peticiones
// para desconectarse.
type Servidor struct {
	entrante chan *Mensaje
	unirse   chan *Conexion
	salir    chan *Conexion
}

// NuevoServidor crea un Servidor que empieza a
// escuchar en sus canales.
func NuevoServidor() *Servidor {
	servidor := &Servidor{
		entrante: make(chan *Mensaje),
		unirse:   make(chan *Conexion),
		salir:    make(chan *Conexion),
	}
	servidor.Escucha()
	return servidor
}

// Escucha inicia una gorutine nueva que escucha en
// los canales del Servidor.
func (servidor *Servidor) Escucha() {
	go func() {
		for {
			select {
			case mensaje := <-servidor.entrante:
				servidor.Maneja(mensaje)
			case conexion := <-servidor.unirse:
				servidor.Conectar(conexion)
			case conexion := <-servidor.salir:
				servidor.Elimina(conexion)
			}
		}
	}()
}

// Conectar se encarga de procesar a los clientes
// que se conectan al servidor.
func (servidor *Servidor) Conectar(conexion *Conexion) {
	log.Printf("Conexión recibida de %v\n"+
		"Serial de Conexion: %v\n", conexion.conn.RemoteAddr(), conexion.serial)
		go conexion.timeoutReset()
	go func() {
		for mensaje := range conexion.entrante {
			servidor.entrante <- mensaje
		}
		servidor.salir <- conexion
	}()
}

// Elimina se encarga de procesar a los clientes
// que se desconectan del servidor.
func (servidor *Servidor) Elimina(conexion *Conexion) {
	close(conexion.saliente)
	log.Printf(EXITO_DESCONECTA, conexion.serial)
}

// Maneja se encarga de procesar los mensajes que
// llegan al servidor. Se da por hecho que el primer
// byte de cada mensaje es el código de la acción,
// por lo que la decisión de qué hacer se toma con base
// en ese byte.
func (servidor *Servidor) Maneja(mensaje *Mensaje) {
	mensaje.conexion.timeout <- true
	if mensaje.conexion.estado == EDO_GRANDE {
		fin := ContinuaRecepcion(mensaje.conexion, mensaje.contenido)
		switch fin {
		case 0:
			log.Printf(EXITO_RECEPCION, mensaje.conexion.usuario.Nombre())
			mensaje.conexion.saliente <- []byte{0x21, 0x0}
		case 1:
			log.Printf(ERROR_RECEPCION, mensaje.conexion.usuario.Nombre())
			mensaje.conexion.saliente <- []byte{0x21, 0x1}
		default:
		}
	} else if mensaje.conexion.estado == EDO_GRANDF {
		fin := ContinuaRecepcionF(mensaje.conexion, mensaje.contenido)
		switch fin {
		case 0:
			log.Printf(EXITO_FOTO, mensaje.conexion.usuario.Nombre())
			mensaje.conexion.saliente <- []byte{0x34, 0x0}
		case 1:
			log.Printf(ERROR_FOTO, mensaje.conexion.usuario.Nombre())
			mensaje.conexion.saliente <- []byte{0x34, 0x1}
		default:
		}
	} else {
		codigo := mensaje.contenido[0]
		switch codigo {
		case 0x0: // 0 Solicitud de registro
			if mensaje.conexion.estado != EDO_INICIO {
				mensaje.conexion.estado = EDO_LOGGED
				log.Printf(ERROR_NO_PERMITIDO, mensaje.conexion.serial)
				mensaje.conexion.saliente <- []byte{0x5C}
			} else {
				usuario := Registra(mensaje.conexion, mensaje.contenido)
				if usuario == nil {
					log.Printf(ERROR_REGISTRO, mensaje.conexion.serial)
					mensaje.conexion.saliente <- []byte{0x5B}
				} else {
					nombre := usuario.Nombre()
					log.Printf(EXITO_REGISTRO, mensaje.conexion.serial, nombre)
					respuesta := append([]byte{0x1}, []byte(nombre)...)
					mensaje.conexion.saliente <- respuesta
				}
			}
		case 0xA: // 10 Solicitud de inicio de sesión
			if mensaje.conexion.estado != EDO_INICIO {
				mensaje.conexion.estado = EDO_LOGGED
				log.Printf(ERROR_NO_PERMITIDO, mensaje.conexion.serial)
				mensaje.conexion.saliente <- []byte{0x5C}
			} else {
				usuario, err := Autentica(mensaje.conexion, mensaje.contenido)
				if err {
					log.Printf(ERROR_AUTENTICACION, mensaje.conexion.serial)
					mensaje.conexion.saliente <- []byte{0x5B}
				} else {
					mensaje.conexion.usuario = usuario
					nombre := usuario.Nombre()
					mensaje.conexion.estado = EDO_LOGGED
					log.Printf(EXITO_AUTENTICACION, mensaje.conexion.serial, nombre)
					respuesta := append([]byte{0x14}, []byte(nombre)...)
					imagen, err := usuario.ObtenerFotoPerfil()
					if err != nil {
						log.Printf(ERROR_INTERNO, err)
						mensaje.conexion.saliente <- []byte{0x5D}
					} else {
						respuesta = append(respuesta, 0xFF)
						respuesta = append(respuesta, ItoByteArray(len(*imagen))...)
						respuesta = append(respuesta, *imagen...)
						mensaje.conexion.saliente <- respuesta
					}
				}
			}
		case 0x1F: // 31 Solicitud de subida de nuevo archivo
			if mensaje.conexion.estado != EDO_LOGGED {
				if mensaje.conexion.estado != EDO_INICIO {
					mensaje.conexion.estado = EDO_LOGGED
				}
				log.Printf(ERROR_NO_PERMITIDO, mensaje.conexion.serial)
				mensaje.conexion.saliente <- []byte{0x5C}
			} else {
				exito, disponible := VerificaEspacio(mensaje.conexion, mensaje.contenido)
				if exito {
					log.Printf(EXITO_ESPACIO, mensaje.conexion.usuario.Nombre(), ByteArraytoI(mensaje.contenido[1:5]))
					mensaje.conexion.saliente <- append([]byte{0x20}, disponible...)
					mensaje.conexion.estado = EDO_ACCARC
				} else {
					log.Printf(ERROR_ESPACIO, mensaje.conexion.usuario.Nombre(), ByteArraytoI(mensaje.contenido[1:5]))
					mensaje.conexion.saliente <- append([]byte{0x22}, disponible...)
				}
			}
		case 0x20: // 32 Recepción de archivo para almacenar
			if mensaje.conexion.estado != EDO_ACCARC {
				if mensaje.conexion.estado != EDO_INICIO {
					mensaje.conexion.estado = EDO_LOGGED
				}
				log.Printf(ERROR_NO_PERMITIDO, mensaje.conexion.serial)
				mensaje.conexion.saliente <- []byte{0x5C}
			} else {
				exito := RecibeArchivo(mensaje.conexion, mensaje.contenido)
				switch exito {
				case 0:
					log.Printf(EXITO_RECEPCION, mensaje.conexion.usuario.Nombre())
					mensaje.conexion.saliente <- []byte{0x21, 0x0}
					mensaje.conexion.estado = EDO_LOGGED
				case 1:
					log.Printf(ERROR_RECEPCION, mensaje.conexion.usuario.Nombre())
					mensaje.conexion.saliente <- []byte{0x21, 0x1}
					mensaje.conexion.estado = EDO_LOGGED
				default:
					log.Printf(EMPIEZA_RECEPCION, mensaje.conexion.usuario.Nombre())
				}
			}
		case 0x29: // 41 Solicitud para ver archivos
			if mensaje.conexion.estado != EDO_LOGGED {
				if mensaje.conexion.estado != EDO_INICIO {
					mensaje.conexion.estado = EDO_LOGGED
				}
				log.Printf(ERROR_NO_PERMITIDO, mensaje.conexion.serial)
				mensaje.conexion.saliente <- []byte{0x5C}
			} else {
				hay, archivos := SolicitaArchivos(mensaje.conexion)
				respuesta := []byte{0x2A}
				if hay {
					mensaje.conexion.estado = EDO_ESPVER
					respuesta = append(respuesta, archivos...)
					mensaje.conexion.saliente <- respuesta
					log.Printf(HAY_ARCHIVOS, mensaje.conexion.usuario.Nombre())
				} else {
					mensaje.conexion.estado = EDO_LOGGED
					mensaje.conexion.saliente <- respuesta
					log.Printf(NO_HAY_ARCHIVOS, mensaje.conexion.usuario.Nombre())
				}
			}
		case 0x2B: // 43 Solicitud para ver un archivo
			if mensaje.conexion.estado != EDO_ESPVER {
				if mensaje.conexion.estado != EDO_INICIO {
					mensaje.conexion.estado = EDO_LOGGED
				}
				log.Printf(ERROR_NO_PERMITIDO, mensaje.conexion.serial)
				mensaje.conexion.saliente <- []byte{0x5C}
			} else {
				exito, archivo := LeeArchivo(mensaje.conexion, mensaje.contenido)
				id := ByteArraytoI(mensaje.contenido[1:5])
				if exito {
					log.Printf(EXITO_ENVIO, id, mensaje.conexion.usuario.Nombre())
					arreglo := append([]byte{0x2C}, ItoByteArray(len(archivo))...)
					arreglo = append(arreglo, archivo...)
					mensaje.conexion.saliente <- arreglo
					time.Sleep(666 * time.Microsecond)
					mensaje.conexion.saliente <- []byte{0x2D, 0x0}
				} else {
					log.Printf(ERROR_ENVIO, id, mensaje.conexion.usuario.Nombre())
					mensaje.conexion.saliente <- []byte{0x2D, 0x1}
				}
				mensaje.conexion.estado = EDO_LOGGED
			}
		case 0x2E:  // 46 Solicitud para eliminar archivo
			if mensaje.conexion.estado != EDO_ESPVER {
				if mensaje.conexion.estado != EDO_INICIO {
					mensaje.conexion.estado = EDO_LOGGED
				}
				log.Printf(ERROR_NO_PERMITIDO, mensaje.conexion.serial)
				mensaje.conexion.saliente <- []byte{0x5C}
			} else {
				exito, id := EliminaArchivo(mensaje.conexion, mensaje.contenido)
				if exito {
					log.Printf(EXITO_ELIMINAR_A, mensaje.conexion.usuario.Nombre(), id)
					mensaje.conexion.saliente <- []byte{0x2F, 0x0}
				} else {
					log.Printf(ERROR_ELIMINAR_A, mensaje.conexion.usuario.Nombre(), id)
					mensaje.conexion.saliente <- []byte{0x2F, 0x1}
				}
				mensaje.conexion.estado = EDO_LOGGED
			}
		case 0x33: // 51 Solicitud cambiar fotografía de perfil
			if mensaje.conexion.estado != EDO_LOGGED {
				if mensaje.conexion.estado != EDO_INICIO {
					mensaje.conexion.estado = EDO_LOGGED
				}
				log.Printf(ERROR_NO_PERMITIDO, mensaje.conexion.serial)
			} else {
				exito := CambiaFoto(mensaje.conexion, mensaje.contenido)
				switch exito {
				case 0:
					log.Printf(EXITO_FOTO, mensaje.conexion.usuario.Nombre())
					mensaje.conexion.saliente <- []byte{0x34, 0x0}
					mensaje.conexion.estado = EDO_LOGGED
				case 1:
					log.Printf(ERROR_FOTO, mensaje.conexion.usuario.Nombre())
					mensaje.conexion.saliente <- []byte{0x34, 0x1}
					mensaje.conexion.estado = EDO_LOGGED
				default:
					log.Printf(EMPIEZA_FOTO, mensaje.conexion.usuario.Nombre())
				}
			}
		case 0x3C: // 60 Solicitud eliminar cuenta
			if mensaje.conexion.estado != EDO_LOGGED {
				if mensaje.conexion.estado != EDO_INICIO {
					mensaje.conexion.estado = EDO_LOGGED
				}
				log.Printf(ERROR_NO_PERMITIDO, mensaje.conexion.serial)
			} else {
				error := mensaje.conexion.usuario.Eliminar()
				if error != nil {
					mensaje.conexion.saliente <- []byte{0x5D}
					log.Printf(ERROR_ELIMINAR_U, mensaje.conexion.usuario.Nombre())
				} else {
					mensaje.conexion.saliente <- []byte{0x3D}
					log.Printf(EXITO_ELIMINAR_U, mensaje.conexion.usuario.Nombre())
					mensaje.conexion.Terminar()
				}
			}
		case 0x50: // 80 Regresar a menú principal
			if mensaje.conexion.estado != EDO_INICIO {
				mensaje.conexion.estado = EDO_LOGGED
				log.Printf(EXITO_ABORTAR, mensaje.conexion.serial)
				mensaje.conexion.saliente <- []byte{0x51}
			} else {
				log.Printf(ERROR_NO_PERMITIDO, mensaje.conexion.serial)
				mensaje.conexion.saliente <- []byte{0x5C}
			}
		case 0x64: // 100 Solicitud para finalizar la conexión
			if mensaje.conexion.estado != EDO_LOGGED &&
				mensaje.conexion.estado != EDO_INICIO {
				mensaje.conexion.estado = EDO_LOGGED
				log.Printf(ERROR_NO_PERMITIDO, mensaje.conexion.serial)
				mensaje.conexion.saliente <- []byte{0x5C}
			} else {
				mensaje.conexion.Terminar()
			}
		default:
			if mensaje.conexion.estado != EDO_LOGGED &&
				mensaje.conexion.estado != EDO_INICIO {
				mensaje.conexion.estado = EDO_LOGGED
			}
			mensaje.conexion.saliente <- []byte{0x5A}
			log.Printf(ERROR_NO_RECONOCIDO, mensaje.conexion.serial, mensaje.contenido)
		}
	}
}
