package servidor

import (
	"bytes"
	"encoding/binary"
	"gitlab.com/alanarteagav/mccloud/pkg/usuarios"
)

// ItoByteArray convierte enteros en arreglos
// de bytes, usando big endian.
func ItoByteArray(x int) []byte {
	y := uint32(x)
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.BigEndian, y)
	return buf.Bytes()
}

// ByteArraytoI convierte arreglos de
// bytes en enteros, usando big endian.
func ByteArraytoI(arreglo []byte) int {
	entero32 := binary.BigEndian.Uint32(arreglo)
	entero := int(entero32)
	return entero
}

// Registra guarda un usuario en la BDD. Aplica
// la función usuarios.CreaUsuario al mensaje
// recibido.
func Registra(conexion *Conexion, contenido []byte) *usuarios.Usuario {
	contenido = contenido[1:]
	partes := bytes.Split(contenido, []byte{255})
	if len(partes) != 2 {
		return nil
	}
	usuario, _ := usuarios.CrearUsuario(string(partes[0]), string(partes[1]), conexion.postgres)
	return usuario
}

// Autentica inicia sesión de un usuario, aplicando
// la función usuarios.Autenticar al mensaje recibido.
func Autentica(conexion *Conexion, contenido []byte) (*usuarios.Usuario, bool) {
	contenido = contenido[1:]
	partes := bytes.Split(contenido, []byte{255})
	if len(partes) != 2 {
		return nil, true
	}
	usuario, err := usuarios.Autenticar(string(partes[0]), string(partes[1]), conexion.postgres)
	if err == 0 {
		return usuario, false
	}
	return nil, true
}

// VerificaEspacio revisa si el usuario tiene
// disponibilidad de espacio para subir un
// archivo.   De ser el caso, cambia de estado
// en la conexión para recibirlo y responde true
// y el espacio disponible. De lo contrario,
// responde false y el espacio disponible.
func VerificaEspacio(conexion *Conexion, contenido []byte) (bool, []byte) {
	archivos, _ := conexion.postgres.ListarArchivos(conexion.usuario.Datos())
	longitud := ByteArraytoI(contenido[1:5])
	disponible := 10000000 // 10485760 cota propuesta de 10Mb
	for _, archivo := range archivos {
		disponible -= archivo.Longitud()
	}
	if longitud < disponible {
		conexion.SetEstado(3)
		arreglo := ItoByteArray(disponible)
		return true, arreglo
	}
	return false, []byte{0, 0, 0, 0}
}

// RecibeArchivo procesa un archivo recibido en
// un mensaje como un arreglo de bytes. Se
// utilizan los paquetes de infraestructura para
// guardar una entrada correspondiente en la base
// de datos y para guardar una copia del archivo
// en el sistema de archivos.
func RecibeArchivo(conexion *Conexion, contenido []byte) int {
	archivos, _ := conexion.postgres.ListarArchivos(conexion.usuario.Datos())
	partesMensaje := bytes.SplitN(contenido, []byte{255}, 2)
	longitud := ByteArraytoI(partesMensaje[1][:4])
	disponible := 10000000 // 10485760 cota propuesta de 10Mb
	for _, archivo := range archivos {
		disponible -= archivo.Longitud()
	}
	objetivo := partesMensaje[1][4:]
	nombre := string(partesMensaje[0][1:])
	if longitud < disponible && len(objetivo) == longitud {
		archivo, _ := conexion.usuario.CrearArchivo(nombre, longitud, &objetivo)
		if archivo != nil {
			return 0
		}
		return 1
	} else if len(objetivo) < longitud {
		conexion.estado = EDO_GRANDE
		conexion.acumulador = append(conexion.acumulador, objetivo...)
		conexion.longitudA = longitud
		conexion.nombreA = nombre
		return 2
	}
	return 1
}

// ContinuaRecepcion continúa la recepción de un archivo
// que llegó en varios mensajes.   Los mensajes recibidos
// son acumulados en el atributo 'acumulador' de la
// estructura conexion asociada a esta sesión del cliente.
// Devuelve un entero; usa 0 para indicar que ya terminó
// de recibir el mensaje y se guardó con éxito; 1 para
// indicar que ya se terminó de recibir el mensaje pero
// hubo un error en la recepción y 2 para indicar que el
// mensaje está incompleto.
func ContinuaRecepcion(conexion *Conexion, contenido []byte) int {
	conexion.acumulador = append(conexion.acumulador, contenido...)
	if conexion.longitudA == len(conexion.acumulador) {
		archivo, _ := conexion.usuario.CrearArchivo(conexion.nombreA, conexion.longitudA, &conexion.acumulador)
		conexion.acumulador = make([]byte, 0)
		conexion.longitudA = 0
		conexion.nombreA = ""
		conexion.estado = EDO_LOGGED
		if archivo != nil {
			return 0
		}
		return 1
	} else if conexion.longitudA < len(conexion.acumulador) {
		conexion.estado = EDO_LOGGED
		return 1
	}
	return 2
}

// LeeArchivo obtiene un archivo a partir de su
// identificador y lo devuelve como un arreglo
// de bytes.
func LeeArchivo(conexion *Conexion, contenido []byte) (bool, []byte) {
	id := ByteArraytoI(contenido[1:5])
	archivo, _ := conexion.usuario.LeerArchivo(id)
	if archivo != nil {
		arreglo := archivo.Bytes()
		return true, *arreglo
	}
	return false, nil
}

// EliminaArchivo elimina un archivo a partir de
// su identificador y reporta un error en caso de
// que no haya sido posible eliminarlo.
func EliminaArchivo(conexion *Conexion, contenido []byte) (bool, int) {
	id := ByteArraytoI(contenido[1:5])
	error := conexion.usuario.EliminarArchivo(id)
	if error != nil {
		return false, id
	}
	return true, id
}

// SolicitaArchivos genera una lista de archivos
// asociados a un usuario para devolverlos como
// un arreglo de bytes.
func SolicitaArchivos(conexion *Conexion) (bool, []byte) {
	archivos, _ := conexion.postgres.ListarArchivos(conexion.usuario.Datos())
	var arreglo []byte
	for _, archivo := range archivos {
		arreglo = append(arreglo, ItoByteArray(archivo.Id())...)
		arreglo = append(arreglo, []byte(archivo.NombreArchivo())...)
		arreglo = append(arreglo, []byte{0xFF}...)
	}
	if len(arreglo) > 0 {
		return true, arreglo
	}
	return false, arreglo
}

// CambiaFoto cambia la foto de perfil de un usuario
// por la que se recibió en la solicitud. Devuelve un
// booleano indicando éxito o error.   La fotografía
// debe tener extensión de archivo de imagen válida
// (gif, jpg o png).
func CambiaFoto(conexion *Conexion, contenido []byte) int {
	partesMensaje := bytes.SplitN(contenido, []byte{255}, 2)
	objetivo := partesMensaje[1][4:]
	extension := string(partesMensaje[0][1:])
	longitud := ByteArraytoI(partesMensaje[1][:4])
	if extension != "gif" && extension != "jpg" && extension != "png" {
		return 1
	}
	extension = "." + extension
	if longitud == len(objetivo) {
		error := conexion.usuario.ActualizarFotoPerfil(&objetivo, extension)
		if error != nil {
			return 1
		}
		return 0
	} else if len(objetivo) < longitud {
		conexion.estado = EDO_GRANDF
		conexion.acumulador = append(conexion.acumulador, objetivo...)
		conexion.longitudA = longitud
		conexion.nombreA = extension
		return 2
	}
	return 1
}

// ContinuaRecepcionF continúa la recepción de una foto de
// perfil que llegó en varios mensajes.   Los mensajes
// recibidos son acumulados en el atributo 'acumulador'
// de la estructura conexion asociada a esta sesión del
// cliente.   Devuelve un entero; usa 0 para indicar que
// ya terminó de recibir el mensaje y se guardó con éxito;
// 1 para indicar que ya se terminó de recibir el mensaje
// pero hubo un error en la recepción y 2 para indicar que
// el mensaje todavía está incompleto.
func ContinuaRecepcionF(conexion *Conexion, contenido []byte) int {
	conexion.acumulador = append(conexion.acumulador, contenido...)
	if conexion.longitudA == len(conexion.acumulador) {
		error := conexion.usuario.ActualizarFotoPerfil(&conexion.acumulador, conexion.nombreA)
		conexion.acumulador = make([]byte, 0)
		conexion.longitudA = 0
		conexion.nombreA = ""
		conexion.estado = EDO_LOGGED
		if error != nil {
			return 1
		}
		return 0
	} else if conexion.longitudA < len(conexion.acumulador) {
		conexion.estado = EDO_LOGGED
		return 1
	}
	return 2
}
