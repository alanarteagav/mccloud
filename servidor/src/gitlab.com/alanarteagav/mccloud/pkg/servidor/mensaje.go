package servidor

import (
	"fmt"
	"time"
)

// Mensaje contiene información sobre el remitente, la hora
// en el que el mensaje fue enviado y el texto del mensaje.
type Mensaje struct {
	hora      time.Time
	conexion  *Conexion
	contenido []byte
}

// NuevoMensaje crea un nuevo mensaje con la hora, la
// conexión y el texto dados.
func NuevoMensaje(hora time.Time, conexion *Conexion, contenido []byte) *Mensaje {
	return &Mensaje{
		hora:      hora,
		conexion:  conexion,
		contenido: contenido,
	}
}

// String regresa una representación del mensaje como cadena
// legible para un humano.
func (mensaje *Mensaje) String() string {
	return fmt.Sprintf("%s - %v: %v\n", mensaje.hora.Format(time.Kitchen), mensaje.conexion.serial, mensaje.contenido)
}
