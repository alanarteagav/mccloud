package servidor

import (
	"bufio"
	"gitlab.com/alanarteagav/mccloud/pkg/datos"
	"gitlab.com/alanarteagav/mccloud/pkg/usuarios"
	"log"
	"net"
	"time"
)

var contadorConexiones = 0

// Conexion contiene la información y las acciones
// que realiza un cliente al conectarse al servidor
// (del lado de servidor).   Tiene un serial
// (identificador numérico consecutivo), un usuario,
// un canal de mensajes entrantes y salientes, y un
// lector y un escritor para comunicarse.   Además,
// cuenta con un estado, y tres campos, acumulador,
// longitudA y nombreA, que funcionan como almacenamiento
// auxiliar para recibir archivos en varios mensajes.
type Conexion struct {
	serial     int
	estado     int
	acumulador []byte
	longitudA  int
	nombreA    string
	entrante   chan *Mensaje
	saliente   chan []byte
	timeout    chan bool
	conn       net.Conn
	lector     *bufio.Reader
	escritor   *bufio.Writer
	postgres   *datos.ConexionPostgres
	usuario    *usuarios.Usuario
}

// NuevaConexion inicializa y devuelve una nueva
// Conexion con la conexión de red dada, inicia
// un lector y un escritor que reciben y envían
// información por el socket.
func NuevaConexion(conn net.Conn) *Conexion {
	escritor := bufio.NewWriter(conn)
	lector := bufio.NewReader(conn)

	conexion := &Conexion{
		serial:     contadorConexiones,
		estado:     0,
		acumulador: make([]byte, 0),
		longitudA:  0,
		nombreA:    "",
		entrante:   make(chan *Mensaje),
		saliente:   make(chan []byte),
		timeout:    make(chan bool, 3),
		conn:       conn,
		lector:     lector,
		escritor:   escritor,
		postgres:   datos.NuevaConexion(),
		usuario:    new(usuarios.Usuario),
	}
	contadorConexiones++
	conexion.Escucha()
	return conexion
}

// Serial devuelve un int, el serial de la conexión.
func (conexion *Conexion) Serial() int {
	return conexion.serial
}

// SetEstado cambia el estado de la conexión. Recibe
// como entrada un int.
func (conexion *Conexion) SetEstado(nuevoEstado int) {
	conexion.estado = nuevoEstado
}

// Lee procesa las cadenas recibidas en el socket de la
// Conexion, les da formato de Mensaje y los envía al
// canal entrante de la Conexion.
func (conexion *Conexion) Lee() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	for {
		receptaculo := make([]byte, 1024)
		n, err := conexion.lector.Read(receptaculo)
		if err != nil {
			break
		}
		contenido := receptaculo[:n]
		message := NuevoMensaje(time.Now(), conexion, contenido)
		conexion.entrante <- message
	}
	close(conexion.entrante)
}

// timeoutReset revisa si se han realizado acciones en los
// últimos TIEMPO_TIMEOUT segundos.   En caso positivo,
// no pasa nada.   En caso negativo, se cierra esta conexión.
func (conexion *Conexion) timeoutReset() {
	for {
		select {
		case reset := <- conexion.timeout:
			if !reset {
				return
			}
		case <- time.After(TIEMPO_TIMEOUT * time.Second):
			log.Printf("El cliente %v excedió el tiempo sin realizar acciones.", conexion.serial)
			if conexion.estado != EDO_INICIO && conexion.estado != EDO_LOGGED {
				regresa := NuevoMensaje(time.Now(), conexion, []byte{0x50})
				conexion.entrante <- regresa
				time.Sleep(666 * time.Microsecond)
				termina := NuevoMensaje(time.Now(), conexion, []byte{0x64})
				conexion.entrante <- termina
			} else if conexion.estado != EDO_INICIO {
				message := NuevoMensaje(time.Now(), conexion, []byte{0x64})
				conexion.entrante <- message
			} else {
				conexion.Terminar()
			}
			return
		}
	}
}

// Escribe procesa los mensajes del canal saliente y
// los escribe en el socket.
func (conexion *Conexion) Escribe() {
	for str := range conexion.saliente {
		_, err := conexion.escritor.Write(str)
		if err != nil {
			log.Println(err)
			break
		}
		err = conexion.escritor.Flush()
		if err != nil {
			log.Println(err)
			break
		}
	}
}

// Escucha inicia dos goroutines, la primera lee del canal
// de salida del cliente y escribe a su socket, y la segunda
// lee del socket del cliente, y escribe en su canal de
// entrada.
func (conexion *Conexion) Escucha() {
	go conexion.Escribe()
	go conexion.Lee()
}

// Terminar cierra la conexión del cliente.
func (conexion *Conexion) Terminar() {
	conexion.timeout <- false
	conexion.conn.Close()
}
