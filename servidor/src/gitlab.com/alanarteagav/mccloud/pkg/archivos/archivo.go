package archivos

import (
    "gitlab.com/alanarteagav/mccloud/pkg/datos"
)

// Estructura Archivo.
// Contiene una representación de sus datos y una conexión a la base de datos.
type Archivo struct {
    bytes    *[]byte
    datos    *datos.DatoArchivo
}

// Establece los bytes de un Archivo.
// Recibe un arreglo de Bytes.
func (archivo *Archivo) SetBytes(bytes *[]byte) {
    archivo.bytes = bytes
}

// Establece los datos de un Archivo.
// Recibe una estructura DatoArchivo.
func (archivo *Archivo) SetDatos(datos *datos.DatoArchivo) {
    archivo.datos = datos
}

// Obtiene los bytes de una estructura Archivo.
// Devuelve un apuntador a un arreglo de Bytes.
func (archivo *Archivo) Bytes() *[]byte {
    return archivo.bytes
}

// Obtiene los datos de un Archivo.
// Devuelve un apuntador a una estructura DatoArchivo.
func (archivo *Archivo) Datos() *datos.DatoArchivo {
    return archivo.datos
}
