package archivos

import (
    "os"
    "io/ioutil"
    "gitlab.com/alanarteagav/mccloud/pkg/errores"
)

// Rutas por omisión del sistema de archivos.
const (
    RAIZ = "./filesystem"
    FOTOSPERFIL = "/fotosPerfil"
    FOTODEFAULT = "./resources/images/default.png"
)

// Verifica si la raiz del sistema de archivos existe.
func ExisteRaiz() bool {
    _, err := os.Stat(RAIZ)
    if os.IsNotExist(err) {
        return false
    }
    return true
}

// Verifica la carpeta de fotos de perfil del sistema de archivos existe.
func ExisteDirectorioFotosPerfil() bool {
    _, err := os.Stat(FOTOSPERFIL)
    if os.IsNotExist(err) {
        return false
    }
    return true
}

// Verifica un directorio existe en el sistema de archivos.
func ExisteDirectorio(directorio string) bool {
    _, err := os.Stat(RAIZ + "/" + directorio)
    if os.IsNotExist(err) {
        return false
    }
    return true
}

// Crea un directorio si no existe.
func CrearDirectorio(directorio string) error {
    if !ExisteDirectorio(directorio){
        err := os.MkdirAll(RAIZ + "/" + directorio, 0755)
		if err != nil {
            return errores.ErrorCreacionDirectorio
		}
    }
    return nil
}

// Crea un directorio si no existe.
func EliminarDirectorio(directorio string) error {
    err := os.RemoveAll(RAIZ + "/" + directorio)
    if err != nil {
        return errores.ErrorEliminacionDirectorio
    }
    return nil
}

// Crea el sistema de archivos si no existe, además crea una imagen de perfil
// por omisión.
func IniciarSistema() error {
    if !ExisteRaiz(){
        err := os.MkdirAll(RAIZ, 0755)
		if err != nil {
            return errores.ErrorCreacionSistema
		}
    }
    if !ExisteDirectorioFotosPerfil(){
        err := os.MkdirAll(RAIZ + FOTOSPERFIL, 0755)
		if err != nil {
            return errores.ErrorCreacionSistema
		}
    }
    bytesFoto, err := ioutil.ReadFile(FOTODEFAULT)
    if err != nil {
        return errores.ErrorLecturaArchivo
    }
    err = CrearArchivo(FOTOSPERFIL + "/" + "default.png", &bytesFoto)
    if err != nil {
        return err
    }
    return nil
}

// Crea un archivo en el sistema de archivos.
// Recibe la ruta del archivo y los bytes del archivo.
func CrearArchivo(rutaArchivo string, bytes *[]byte) error {
    archivo, err := os.Create(RAIZ + "/" + rutaArchivo)
    if err != nil {
        return errores.ErrorCreacionArchivo
    }
    defer archivo.Close()
    _, err = archivo.Write(*bytes)
    if err != nil {
        return errores.ErrorEscrituraArchivo
    }
    return nil
}

// Lee un archivo del sistema de archivos.
// Recibe la ruta del archivo.
// Devuelve un arreglo de bytes del archivo.
func LeerArchivo(rutaArchivo string) (*[]byte, error) {
    contenido, err := ioutil.ReadFile(RAIZ + "/" + rutaArchivo)
    if err != nil {
        return nil, errores.ErrorLecturaArchivo
    }
    return &contenido, nil
}

// Elimina un archivo del sistema de archivos.
// Recibe la ruta del archivo.
func EliminarArchivo(rutaArchivo string) (error) {
    err := os.Remove(RAIZ + "/" + rutaArchivo)
    if err != nil {
       return errores.ErrorEliminacionArchivoSistema
    }
    return nil
}
