# Script Bash para configurar servidor McCloud.
export GOPATH=$(pwd)/servidor
export GOBIN=$(pwd)/servidor/bin

# Remove BIN DIR
if [ -e ./servidor/bin/ ]
then
    echo "[ REMOVING BIN DIR ]"
    rm -r servidor/bin/
fi

# Remove BIN DIR
if [ -e ./servidor/pkg/ ]
then
    echo "[ REMOVING PKG DIR ]"
    rm -r servidor/pkg/
fi

cd servidor/src/gitlab.com/alanarteagav/mccloud/

go get ./...
go build ./...
go install ./...

cd ..
cd ..
cd ..
cd ..
cd ..
