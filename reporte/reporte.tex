\documentclass{article}

\usepackage[utf8]{inputenc}

% Symbols
\usepackage{amsmath}
\usepackage{amssymb}

% Language
% \usepackage[spanish]{babel}

% Size of boxes
\usepackage{adjustbox}

% Columns for tables
\usepackage{array}
\newcommand{\PreserveBackslash}[1]{\let\temp=\\#1\let\\=\temp}
\newcolumntype{C}[1]{>{\PreserveBackslash\centering}p{#1}}
\newcolumntype{R}[1]{>{\PreserveBackslash\raggedleft}p{#1}}
\newcolumntype{L}[1]{>{\PreserveBackslash\raggedright}p{#1}}

% Float managment
\usepackage{float}

% Color
\usepackage[table]{xcolor}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\definecolor{verylightgray}{rgb}{0.92,0.92,0.92}

% Links
\usepackage{hyperref}
\hypersetup{
  colorlinks,
  urlcolor={mauve}
}

% Code-like text
\usepackage{listings}
\lstset{
  language=bash,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  extendedchars=true,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3,
  backgroundcolor = \color{verylightgray},
}


% Margins
\usepackage{geometry}
\addtolength{\hoffset}{-0.5cm}
\addtolength{\textwidth}{1cm}
\addtolength{\voffset}{-0.5cm}
\addtolength{\textheight}{1.9cm}
\addtolength{\headsep}{0.5cm}

% Graphics
\usepackage{graphicx}

% Drawings
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,matrix,decorations.pathmorphing,
                shapes.geometric,calc,babel}


% Header-Footer
\usepackage{fancyhdr}
\pagestyle{fancyplain}
\lhead{Alan Ernesto Arteaga V\'azquez \\
       Mauricio Carrasco Ruiz \\
       C\'esar Hern\'andez Cruz}
\chead{Redes de Computadoras \\ Proyecto 2}
\rhead{Fecha de entrega: \\ 2 de febrero de 2021}

\renewcommand\headrulewidth{1.5pt}
\makeatletter
\def\headrule{
{\if@fancyplain\let\headrulewidth\plainheadrulewidth\fi
\hrule\@height\headrulewidth\@width\headwidth
\vskip 2pt% 2pt between lines
\hrule\@height.5pt\@width\headwidth% lower line w/.5pt line width
\vskip-\headrulewidth
\vskip-1.5pt}}
\makeatother

% Macros
\newcommand{\ttt}[1]{%
\texttt{#1}%
}

\newcommand{\blueCell}{%
\cellcolor{blue!10}%
}

\newcommand{\redCell}{%
\cellcolor{red!10}%
}

\begin{document}

% ===== [Sección 1] == Diseño del protocolo} ==================================
\section{Diseño del protocolo}

En esta secci\'on se justifican las decisiones tomadas para
la implementaci\'on del protocolo, y se describen las
caracter\'isticas que fueron agregadas.

\subsection{Aut\'omata}

Iniciamos con el dise\~no del aut\'omata, que es el aut\'omata
presentado en la especificaci\'on del proyecto, extendido con
algunos estados y mensajes adicionales.   No se reproducir\'a
la tabla de la especificaci\'on, s\'olo discutiremos los estados
nuevos en la Tabla \ref{tab:estados}

\begin{table}[H]
\centering
\begin{tabular}{|c|l|}
\hline
S11 & Recibe una solicitud para eliminar un archivo.                \\
\hline
S12 & Recibe una solicitud para cambiar la foto de perfil.          \\
\hline
S13 & Recibe una solicitud para eliminar permanentemente la cuenta. \\
\hline
\end{tabular}
\caption{Tabla de estados nuevos para el protocolo.}
\label{tab:estados}
\end{table}

En particular, el estado S11 reutiliza de manera natural el
hecho de ya haber recibido una lista de archivos, a partir
de la cu\'al puede elgir uno, ya sea para descargar, o para
eliminar.

En el aut\'omata se muestran algunos mensajes de error, aunque
en todos los estados, al solicitar una acci\'on no permitida,
se env\'ia un mensaje de error que, en la mayor\'ia de los
casos, devuelve al aut\'omata al estado S2 (a menos que el
cliente no hubiera iniciado sesi\'on, en cuyo caso se queda
en el estado S0).

\begin{figure}[H]
  \centering
  \includegraphics[width=1\textwidth]{aut/Automata}
  \caption{El aut\'omata que representa los estados del sistema.}
  \label{fig:automata}
\end{figure}


\subsection{Nuevas caracter\'isticas}

Adem\'as de las caracter\'isticas mencionadas en la
especificaci\'on del proyecto, nuestro sistema cuenta
con las siguientes caracter\'isticas adicionales.

\begin{enumerate}
  \item \textbf{Registro de nuevos usuarios:} Un nuevo usuario
    puede solicitar su registro al servidor para utilizar la
    aplicaci\'on.  Al momento de registrarse, el usuario s\'olo
    proveer\'a un nombre de usuario y constrase\~na; en caso de
    que el nombre elegido ya est\'e utilizado, se le indicar\'a
    al usuario para que elija uno nuevo, y en otro caso, el
    registro proceder\'a.   Habiendo iniciado sesi\'on, el usuario
    puede cambiar su imagen de perfil.

  \item \textbf{Notificaci\'on al usuario de espacio disponible:}
    Como parte de la implementaci\'on de la condici\'on de tama\~no
    de almacenamiento permitido, se agreg\'o un mensaje de respuesta
    al usuario cuando desea subir un archivo, indic\'andole si tiene,
    o no, espacio disponible.

  \item \textbf{Solicitud para eliminar un archivo:} Considerando
    que el usuario tiene una restricci\'on de espacio, es natural
    considerar el caso en el que este se ha agotado, y para poder
    seguir utilizando la aplicaci\'on, el usuario necesita la opci\'on
    de eliminar archivos.   Esta funcionalidad se puede agregar
    f\'acilmente en el mismo flujo que ``solicitar archivos'', tras
    haber recibido la lista de los archivos en el servidor.

  \item \textbf{Solicitud para cambiar imagen de perfil:} Al contar
    con una imagen de perfil es necesario contar tambi\'en con un
    mecanismo para cambiarla.   En particular, como al registrarse
    el usuario no elige una imagen de perfil, el servidor le
    proporciona una por omisi\'on.

  \item \textbf{Solicitud para eliminar cuenta:} Se entiende que no
    todos los usuarios se sentir\'an c\'omodos usando la aplicaci\'on,
    por lo que es razonable contar con una forma de eliminar la cuenta
    de un usuario, eliminando su informaci\'on de inicio de sesi\'on
    de la base de datos, as\'i como todos sus archivos del sistema de
    archivos y de la base de datos.
\end{enumerate}





\subsection{Mensajes del protocolo}

Los mensajes permitidos en el protocolo se muestran en la
Tabla \ref{tab:mensajes}.   Los mensajes resaltados en
azul son los que agregamos al protocolo base definido en
las especificaciones del proyecto.   Los mensajes resaltados
aparecen en el protocolo original, pero fueron modificados.

\begin{table}[ht!]
\begin{adjustbox}{max width=1\textwidth}
\begin{tabular}{|c|l|C{4cm}|c|}
\hline
  C\'odigo
& Descripci\'on
& Longitud en bytes
& Forma \\
\hline
  0 \blueCell
& Solicitud al servidor para registrarse \blueCell
& 1 + usuario + contrase\~na \blueCell
& [0$|$usuario$|$contrase\~na]  \blueCell\\
\hline
  1 \blueCell
& Respuesta indicando lo sucedido \blueCell
& 2 \blueCell
& [1$|$0] para \'exito o [1$|$1] para error \blueCell \\
\hline
  10
& Solicitud al servidor para iniciar sesi\'on
& 1 + usuario + contrase\~na
& [10$|$usuario$|$contrase\~na] \\
\hline
  20 \redCell
& Respuesta al cliente con su perfil \redCell
& 1 + nombre + Long(imagen) + perfil \redCell
& [20$|$nombre$|$longitud$|$imagen] \redCell \\
\hline
  31
& Solicitud de subida de un nuevo archivo
& 1
& [31] \\
\hline
  32
& Env\'io de archivo a almacenar
& 1 + nombre + Long(archivo) + archivo
& [32$|$nombre$|$longitud$|$archivo] \\
\hline
  33
& Respuesta indicando lo sucedido
& 2
& [33$|$0] para \'exito o [33$|$1] para error \\
\hline
  34 \blueCell
& Espacio disponible \blueCell
& 1 + 4 \blueCell
& [34$|$espacio disponible] \blueCell \\
\hline
  41
& Solicitud para ver archivos
& 1
& [41] \\
\hline
  42
& Respuesta con una lista de IDs de archivos
& 1 + (4 + nombre) $\ast$ n\'umeroArchivos
& [42$|$ID1$|$nombre1$|$ID2$|$nombre2$|$...] \\
\hline
  43
& Solicitud para ver un archivo
& 5
& [43$|$ID] \\
\hline
  44
& Env\'io del archivo solicitado
& 1 + Long(archivo) + archivo
& [44$|$longitud$|$archivo] \\
\hline
  45
& Respuesta indicando lo sucedido
& 2
& [45$|$0] para \'exito o [45$|$1] para error \\
\hline
  46 \blueCell
& Solicitud para eliminar un archivo \blueCell
& 5 \blueCell
& [46$|$ID] \blueCell \\
\hline
  47 \blueCell
& Respuesta indicando lo sucedido \blueCell
& 2 \blueCell
& [47$|$0] para \'exito o [47$|$1] para error \blueCell \\
\hline
  51 \blueCell
& Solicitud cambiar imagen perfil \blueCell
& 1 + 3 + Long(archivo) + archivo \blueCell
& [51$|$extension$|$longitud$|$archivo] \blueCell \\
\hline
  52 \blueCell
& Respuesta indicando lo sucedido \blueCell
& 2 \blueCell
& [52$|$0] para \'exito o [52$|$1] para error \blueCell \\
\hline
  60 \blueCell
& Solicitud eliminar cuenta \blueCell
& 1  \blueCell
& [60] \blueCell \\
\hline
  61 \blueCell
& Respuesta exitosa a solicitud eliminar cuenta \blueCell
& 1  \blueCell
& [61] \blueCell \\
\hline
  80 \blueCell
& Abortar el flujo actual y regresar al estado inicial \blueCell
& 1  \blueCell
& [80] \blueCell \\
\hline
  90 \blueCell
& Comando no reconocido \blueCell
& 1  \blueCell
& [90] \blueCell \\
\hline
  91 \blueCell
& Fallo de autenticaci\'on o registro \blueCell
& 1  \blueCell
& [91] \blueCell \\
\hline
  92 \blueCell
& Comando no permitido \blueCell
& 1  \blueCell
& [92] \blueCell \\
\hline
  93 \blueCell
& Error interno del servidor \blueCell
& 1  \blueCell
& [93] \blueCell \\
\hline
  100
& Solicitud para finalizar la conexi\'on
& 1
& [100] \\
\hline
\end{tabular}
\end{adjustbox}
\caption{Mensajes permitidos en el protocolo.}
\label{tab:mensajes}
\end{table}

Eligimos utilizar como delimitador el byte \ttt{0x255},
que no corresponde a un car\'acter imprimible.   No es
necesario usar el delimitador en todos los casos; a
continuaci\'on mencionamos aquellos donde se utiliz\'o.
\begin{itemize}
  \item[0] Entre \ttt{usuario} y \ttt{contrase\~na}.

  \item[10] Entre \ttt{usuario} y \ttt{contrase\~na}.

  \item[20] Entre \ttt{nombre} y \ttt{longitud}.

  \item[32] Entre \ttt{nombre} y \ttt{longitud}.

  \item[42] Entre el $i$-\'esimo \ttt{nombre} y el
    $(i+1)$-\'esimo \ttt{ID}.

  \item[51] Entre \ttt{extension} y \ttt{longitud}. (Este
    delimitador es innecesario, pero es necesario
    refactorizar los m\'etodos de env\'io y manejo de
    esta solicitud para eliminarlo.)
\end{itemize}

Adem\'as, se agregaron c\'odigos de error para el
protocolo.   A continuaci\'on explicamos brevemente
en qu\'e caso se utiliza cada uno de ellos.
\begin{itemize}
  \item[90] Como respuesta a cualquier mensaje cuyo formato
    no aparezca en la Tabla \ref{tab:mensajes}.

  \item[91] Cuando falle el inicio de sesi\'on de un cliente,
    o su sesi\'on de registro.   El primero fallar\'a cuando
    la pareja \ttt{usuario}-\ttt{contrase\~na} no se encuentre
    en la BDD.  El segundo fallar\'a cuando ya exista un usuario
    registrado con el mismo nombre.

  \item[92] Como respuesta a cualquier mensaje que se reciba
    en un estado distinto al esperado, seg\'un la Figura
    \ref{fig:automata}.

  \item[93] Cuando se presenta alg\'un error en el sistema de
    archivos o en la base de datos que impida procesar una
    solicitud.
\end{itemize}

Vale la pena resaltar que el c\'odigo 90 nunca se enviar\'a
cuando el servidor se utiliza con nuestro cliente, pues el
cliente s\'olo enviar\'a mensajes permitidos.   De manera
similar, tampoco se obtendr\'a como respuesta el c\'odigo
92, pues el cliente tendr\'a un estado que refleja el estado
del servidor, por lo que el usuario s\'olo tendr\'a a su
disposici\'on un conjunto de acciones permitidas.   Respecto
a este \'ultimo punto, llamamos la atenci\'on sobre el
mensaje con c\'odigo 80, que est\'a disponible desde cualquier
estado (excepto antes de iniciar sesi\'on), y cuya finalidad
es regresar al servidor al estado inicial (justo despu\'es de
iniciar sesi\'on).  Desde el punto de vista del cliente, esto
\'ultimo se visualizar\'a como ``volver al men\'u principal''.

% ===== [Sección 2] == Uso de la aplicación} ==================================
\section{Uso de la aplicación}

Las instrucciones para iniciar la aplicaci\'on, tanto
el cliente como el servidor, se encuentran en el
archivo \ttt{README.md} en la
\href{https://gitlab.com/alanarteagav/mccloud}{p\'agina
principal del proyecto}.   La documentaci\'on del
servidor se puede encontrar
\href{https://pkg.go.dev/gitlab.com/alanarteagav/mccloud}{en
este enlace}.   La documentaci\'on del cliente puede
encontrarse en la carpeta \ttt{cliente/documentacion}.

A continuaci\'on se muestran algunas capturas de pantalla
que muestran el uso de la aplicaci\'on desde la interfaz
de l\'inea de comandos.   La documentaci\'on de flujos
distintos al normal no es exhaustiva, pues la mayor\'ia
se manejan de una forma similar.

\begin{itemize}
  \item Flujo normal de registro e inicio de sesi\'on.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{uso/registro-inicio-n}
      \caption{Solicitud de registro de nuevo usuario e inicio de sesi\'on.}
      \label{fig:registro-inicio}
    \end{figure}

  \item Flujo normal para subir un archivo.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{uso/subir-n}
      \caption{Solicitud para subir un archivo al servidor.}
      \label{fig:registro-inicio}
    \end{figure}

  \item Solicitud de lista de archivos.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{uso/ver}
      \caption{Lista de archivos en el servidor.}
      \label{fig:ver}
    \end{figure}

  \item Regresar a men\'u principal desde lista de archivos.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{uso/abortar-mision}
      \caption{Regresando a men\'u principal.}
      \label{fig:abortar-mision}
    \end{figure}

  \item Flujo normal para descargar un archivo.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.8\textwidth]{uso/descargar}
      \caption{Descargar un archivo.}
      \label{fig:descargar}
    \end{figure}

  \item Flujo normal para eliminar un archivo.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.5\textwidth]{uso/eliminar-a}
      \caption{Eliminar un archivo.}
      \label{fig:eliminar-a}
    \end{figure}

  \item Flujo con error al intentar descargar un archivo.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{uso/descargar-e}
      \caption{Error al intentar descargar un archivo.}
      \label{fig:descargar-e}
    \end{figure}

  \item Ver imagen de perfil.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.7\textwidth]{uso/perfil}
      \caption{Ver la imagen de perfil.}
      \label{fig:perfil}
    \end{figure}

  \item Cambiar imagen de perfil.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.8\textwidth]{uso/cambiar-perfil}
      \caption{Cambiar la imagen de perfil.}
      \label{fig:cambiar-perfil}
    \end{figure}

  \item Solicitud para eliminar cuenta.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{uso/eliminar-u}
      \caption{Eliminar permanentemente la cuenta.}
      \label{fig:eliminar-u}
    \end{figure}

  \item Solicitud para cerrar sesi\'on.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.5\textwidth]{uso/salir}
      \caption{Cerrar sesi\'on.}
      \label{fig:salir}
    \end{figure}
\end{itemize}

% ===== [Sección 3] == Funcionamiento y capturas de pantalla con Wireshark} ===
\section{Funcionamiento y capturas de pantalla con Wireshark}

A continuaci\'on se muestran las capturas de pantalla del
tr\'afico de red generado por los mensajes intercambiados
entre el cliente y el servidor en un flujo normal de acciones.

\begin{itemize}
  \item Solicitud de registro.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/0}
      \caption{Solicitud de registro de nuevo usuario.}
      \label{fig:0}
    \end{figure}

  \item Respuesta positiva a solicitud de registro.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/1}
      \caption{Respuesta positiva del servidor a solicitud de
      registro de nuevo usuario.}
      \label{fig:1}
    \end{figure}

  \item Solicitud de inicio de sesi\'on.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/10}
      \caption{Solicitud de registro de nuevo usuario.}
      \label{fig:10}
    \end{figure}

  \item Respuesta positiva a solicitud de inicio de sesi\'on.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/20}
      \caption{Respuesta positiva del servidor a solicitud de
      registro de nuevo usuario.}
      \label{fig:20}
    \end{figure}

  \item Respuesta negativa a solicitud de registro o inicio de sesi\'on.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/91}
      \caption{Respuesta negativa del servidor a solicitud de
      registro de nuevo usuario o inicio de sesi\'on.}
      \label{fig:20}
    \end{figure}

  \item Solicitud para subir un archivo.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/31}
      \caption{Solicitud para subir un archivo.}
      \label{fig:31}
    \end{figure}

  \item Respuesta positiva a petici\'on para subir un archivo.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/32s}
      \caption{Respuesta positiva a solicitud para subir un archivo.}
      \label{fig:32s}
    \end{figure}

  \item Respuesta negativa a petici\'on para subir un archivo.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/32s-1}
      \caption{Respuesta negativa a solicitud para subir un archivo.}
      \label{fig:32s-1}
    \end{figure}

  \item Env\'io del archivo para subir.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/32c}
      \caption{Env\'io del archivo para subir.}
      \label{fig:32c}
    \end{figure}

  \item Confirmaci\'on de recepci\'on correcta del archivo.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/33-0}
      \caption{Recepci\'on correcta del archivo.}
      \label{fig:33-0}
    \end{figure}


  \item Solicitud para ver los archivos que hay almacenados.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/41}
      \caption{Solicitud para ver los archivos almacenados.}
      \label{fig:41}
    \end{figure}

  \item Respuesta con los archivos almacenados en servidor.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/42-0}
      \caption{Solicitud para ver los archivos almacenados.}
      \label{fig:42-0}
    \end{figure}

  \item Solicitud para descargar un archivo.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/43}
      \caption{Solicitud para descargar un archivo.}
      \label{fig:43}
    \end{figure}

  \item Respuesta positiva a solicitud para descargar un archivo.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/44}
      \caption{Respuesta positiva a solicitud para descargar un archivo.}
      \label{fig:44}
    \end{figure}

  \item Solicitud para eliminar un archivo.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/46}
      \caption{Solicitud para eliminar un archivo.}
      \label{fig:46}
    \end{figure}

  \item Respuesta positiva a solicitud para eliminar un archivo.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/47-0}
      \caption{Respuesta positiva a solicitud para eliminar un archivo.}
      \label{fig:47-0}
    \end{figure}

  \item Respuesta negativa a solicitud para eliminar un archivo.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/47-1}
      \caption{Respuesta negativa a solicitud para eliminar un archivo.}
      \label{fig:47-1}
    \end{figure}

  \item Solicitud de cambio de imagen de perfil.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/51}
      \caption{Solicitud de cambio de perfil.}
      \label{fig:51}
    \end{figure}

  \item Respuesta positiva a solicitud de cambio de imagen de perfil.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/52}
      \caption{Respuesta positiva a solicitud de cambio de perfil.}
      \label{fig:52}
    \end{figure}

  \item Solicitud para eliminar cuenta.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/60}
      \caption{Solicitud para eliminar cuenta.}
      \label{fig:60}
    \end{figure}

  \item Respuesta a solicitud para eliminar cuenta.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/61}
      \caption{Solicitud para eliminar cuenta.}
      \label{fig:61}
    \end{figure}

  \item Solicitud para regresar al men\'u principal.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/80}
      \caption{Solicitud para regresar al men\'u principal.}
      \label{fig:80}
    \end{figure}

  \item Confirmaci\'on de regreso a estado inicial.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/81}
      \caption{Confirmaci\'on de regreso al men\'u principal.}
      \label{fig:81}
    \end{figure}

  \item Solicitud para terminar sesi\'on.
    \begin{figure}[H]
      \centering
      \includegraphics[width=0.6\textwidth]{ws/100}
      \caption{Solicitud para terminar sesi\'on.}
      \label{fig:100}
    \end{figure}

\end{itemize}

\end{document}
