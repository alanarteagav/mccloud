"""
Una clase para los archivos.

Para generar la documentación en HTML de este
módulo, ejecutar el siguiente comando:

    pydoc -w src.Archivo
"""

class Archivo(object):
    """
    Un archivo tiene nombre, tamaño 
    y contenidos
    
    Encapsula los siguientes atributos:
    
    * name
        El nombre del archivo.
    * size
        El tamaño del archivo.
    * contents
        Los contenidos del archivo.
    """

    name     = None
    size     = None
    contents = None
    
    def __init__(self, _name, _size):
        """
        Construye un archivo, recibiendo el 
        nombre y el tamaño del archivo.
        :param _name: El nombre del archivo.
        :param _size: El tamaño del archivo.
        """
        self.name = _name
        self.size = _size

    def set_name(self, _name):
        """
        Le asigna un nuevo nombre al archivo.
        """
        self.name = _name

    def extender(self, content):
        """
        Extiende el contenido del archivo, 
        adjuntando hasta el final los contenidos
        pasados por el argumento.
        :param content: Los contenidos a adjuntar.
        """
        if (self.contents is None):
            self.contents = bytearray(content)
        else:
            self.contents.extend(content)

    def esta_completo(self):
        """
        Regresa la cantidad de bytes faltantes o 
        sobrantes que tiene el archivo.
        :return: La cantidad de bytes faltantes o sobrantes.
        """
        return (len(self.contents) - self.size)

    def guardar(self):
        """
        Guarda el archivo en el sistema del usuario.
        """
        try:
            f = open('./' + self.name, 'wb')
            f.write(self.contents)
            f.close()

        except Exception as e:
            raise e
