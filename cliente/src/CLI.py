from socket import error as SocketError
from src.Cliente import *
from src.ClienteEstado import *
from threading import Thread

import socket
import subprocess
import sys

"""
Una clase para la interfaz de linea de comandos
(Command Line Interface, CLI).

Para generar la documentación en HTML de este
módulo, ejecutar el siguiente comando:

    pydoc -w src.CLI
"""

class CLI(object):
    """
    La interfaz de línea de comandos será
    el intermediario entre el cliente y el
    servidor, mostrándole al cliente lo que
    el servidor le responde en formate legible
    y entendible.

    Encapsula los siguientes atributos:

    * Cliente
        El cliente.
    * Comunicacion
        El canal de comunicación que se usa con el servidor.
    * file_transfer
        El archivo que se solicitará subir al servidor.
    * image_viewer
        El programa para abrir imágenes dependiendo del sistema
        operativo.
    """

    client        = None
    comunicacion  = None
    file_transfer = None

    def __init__(self):
        """
        Constructor para el CLI.
        """
        self.cliente = Cliente()
        self.cliente.set_escucha(self.mensaje_del_servidor)
        self.cliente.set_estado(ClienteEstado.INIT)

        os = {'linux':'viewnior', 'darwin':'open'}
        self.image_viewer = os[sys.platform]

    def bienvenida(self):
        """
        Le da la bienvenida al cliente.
        """
        s  = "McCloud 0.0.0 \n"
        print(s)

        self.cliente.set_estado(ClienteEstado.CONN)

    def conectar_servidor(self):
        """
        Le indica al cliente que introduzca la
        dirección y puerto del servidor al que
        se quiere conectar.
        :return: True si la conexión fue exitosa, False en otro caso.
        """
        s  = "Coloca la dirección y puerto del servidor al "
        s += "que te te quieres conectar. \n"
        print(s)

        address = input("Dirección: ")
        port    = input("Puerto: ")

        print("\nTratando de conectarse al servidor... \n")

        try:
            self.cliente.set_address(address)
            self.cliente.set_port(port)
            self.cliente.conectar()

            # Empieza a escuchar del servidor.
            self.comunicacion = Thread(target = self.cliente.escucha_servidor,
                                       daemon = True)
            self.comunicacion.start()

            print("Conexión establecida con éxito.\n")

            return True

        except ValueError as e:
            print("Error: Asegúrate de introducir un número en el puerto.\n")
            return False

        except OverflowError as e:
            err_msg  = "Error: El puerto debe de ser un número entre 0 y 65535. "
            err_msg += "¿Pusiste el puerto correcto?\n"

            print(err_msg)
            return False

        except socket.timeout as e:
            err_msg  = "Error: Se agotó el tiempo de espera para "
            err_msg += "conectarse al servidor especificado. "
            err_msg += "¿Pusiste la dirección y el puerto correctos?\n"

            print(err_msg)
            return False

        except socket.error as e:
            err_msg = ""

            if (e.errno == -2):
                err_msg  = "Error: Dirección desconocida y/o inválida. "
                err_msg += "¿Pusiste la dirección correcta?\n"

            elif (e.errno == 22) or (e.errno == 103):
                err_msg  = "Error: La dirección y el puerto son correctos "
                err_msg += "pero se esta rechazando la conexión.\n"
                err_msg += "Se va a cerrar el programa. Abre de nuevo el "
                err_msg += "cliente para intentar de nuevo la conexión.\n"
                err_msg += "Disculpa las molestias.\n"
                self.cliente.set_estado(ClienteEstado.S10)

            elif (e.errno == 61):
                err_msg  = "Error: La conexión ha sido rechazada. "
                err_msg += "¿Pusiste la dirección/puerto correcto?"

            elif (e.errno == 111):
                err_msg  = "Error: La conexión ha sido rechazada. "
                err_msg += "¿Pusiste el puerto correcto?\n"

            else:
                print(e)

            print(err_msg)
            return False

    def intentar_conexion(self):
        """
        El cliente intenta conectarse multiple veces al
        servidor en caso de que haya un error al conectarse.
        """
        while True:
            success = self.conectar_servidor()
            _estado = self.cliente.get_estado()

            if (success):
                self.cliente.set_estado(ClienteEstado.S0)
                return

            elif (_estado == ClienteEstado.S10):
                return

            else:
                print("¿Quieres intentar conectarte al servidor de nuevo?")
                ans = input("Respuesta[S/N]: ")
                if (ans == "N"):
                    self.cliente.set_estado(ClienteEstado.S10)
                    return

    def inicia_o_registra(self):
        """
        Le pregunta al cliente si ya tiene una cuenta,
        sino para registrarlo.
        """
        print("¿Ya tienes una cuenta en McCloud?")
        ans = input("Respuesta[S/N]: ")

        while (self.cliente.is_connected):
            if (ans == "S"):
                self.cliente.set_estado(ClienteEstado.S1)
                return
            elif (ans == "N"):
                self.cliente.set_estado(ClienteEstado.SA)
                break
            else:
                print("Por favor escribe S ó N")
                ans = input("Respuesta[S/N]: ")

    def registrarse(self):
        """
        Le indica al cliente que introduzca los
        datos (nombre de usuario y contraseña)
        con los que se quiere registrar en el
        servidor.
        """
        print("\nIntroduce los siguientes datos para registrarte:")
        user = input("Usuario: ")
        pw   = input("Contraseña: ")

        try:
            self.cliente.registrar(user, pw)

            while (self.cliente.is_connected):
                _estado = self.cliente.get_estado()

                if (_estado == ClienteEstado.EXITO):
                    print("\n¡Se ha registrado con éxito!")
                    break

                elif (_estado == ClienteEstado.ERROR):
                    print("\nError: El usuario ya existe.")
                    break

            self.cliente.set_estado(ClienteEstado.S1)

        except socket.error as error:
            if (error.errno == 9):
                print("Error: Se perdió la comunicación con el servidor.")
            else:
                print(str(e))


    def iniciar_sesion(self):
        """
        Le indica al cliente que introduzca sus
        credenciales (nombre de usuario y contraseña)
        para iniciar sesión en el servidor.
        """
        print("\nIntroduzca los siguientes datos para iniciar sesión:")
        user = input("Usuario: ")
        pw   = input("Contraseña: ")

        try:
            self.cliente.inicia_sesion(user, pw)

            # Espera a que el servidor responda.
            while (self.cliente.is_connected):
                _estado = self.cliente.get_estado()

                if (_estado == ClienteEstado.S2):
                    msg = "\nBienvenido " +  self.cliente.get_name()
                    print(msg)
                    break

                elif (_estado == ClienteEstado.ERROR):
                    msg = "\nError: Usuario y/o contraseña incorrectos."
                    print(msg)
                    self.cliente.set_estado(ClienteEstado.S10)
                    break

        except socket.error as error:
            if (error.errno == 9):
                print("Error: Se perdió la comunicación con el servidor.")
            else:
                print(str(e))

    def mostrar_acciones(self):
        """
        Le muestra al cliente las acciones que puede
        realizar.
        """
        s  = "Introduzca el número de la acción que "
        s += "quiere realizar. \n"
        s += "  1) Subir un archivo. \n"
        s += "  2) Ver los archivos que hay almacenados. \n"
        s += "  3) Ver foto de perfil. \n"
        s += "  4) Cambiar foto de perfil \n"
        s += "  5) Cerrar la conexión y salir. \n"
        s += "  6) Eliminar mi cuenta. \n"

        print(s)

        accion = input("Acción: ")

        if (accion == "1"):
            self.cliente.set_estado(ClienteEstado.S3)

        elif (accion == "2"):
            self.cliente.set_estado(ClienteEstado.S5)
        
        elif (accion == "3"):
            self.mostrar_imagen("perfil")

        elif (accion == "4"):
            self.cliente.set_estado(ClienteEstado.S11)

        elif (accion == "5"):
            self.cliente.set_estado(ClienteEstado.S9)

        elif (accion == "6"):
            self.cliente.set_estado(ClienteEstado.S13)

        else:
            self.cliente.set_estado(ClienteEstado.S2)
            print("Acción no reconocida.\n")

    def solicitar_subida(self):
        """
        Le manda un mensaje el servidor para solicitar la
        subida de un archivo.
        """
        try:
            file_path = input("Introduzca la ubicación del archivo: ")

            self.file_transfer = file_path

            print("\nIndicandole al servidor que se va a subir un archivo...")
            print("Preguntándole al servidor si hay espacio suficiente...\n")
            self.cliente.solicitud_para_archivo(file_path)

            while (self.cliente.is_connected):
                _estado = self.cliente.get_estado()

                if (_estado == ClienteEstado.S4):
                    print("Tienes espacio suficiente.\n")
                    return
                elif (_estado == ClienteEstado.S2):
                    print("Error: No tienes espacio suficiente.\n")
                    return

        except Exception as e:
            if (e.errno == 2):
                err_msg  = "Error: El archivo que solicitaste subir no existe."
                err_msg += " ¿Pusiste la ubicación correcta?\n"
                print(err_msg)
                
                self.cliente.set_estado(ClienteEstado.S2)

            elif (e.errno == 9):
                print("Error: Se perdió la comunicación con el servidor.")
                self.cliente.set_estado(ClienteEstado.LOSS_CONN)

            else:
                print(str(e))
                self.cliente.set_estado(ClienteEstado.S2)

    def subir_archivo(self):
        """
        Le manda un mensaje al servidor para indicarle
        que se va a subir un archivo y le pide los
        datos necesarios (ubicación del archivo y nombre)
        al cliente.
        """
        try:
            name = input("Introduzca el nombre con el que desea guardar el archivo: ")

            print("\nEnviando archivo...")
            self.cliente.enviar_archivo(name, self.file_transfer)

            while True:
                _estado = self.cliente.get_estado()

                if (_estado == ClienteEstado.EXITO):
                    print("Archivo guardado con éxito.\n")
                    break
                elif (_estado == ClienteEstado.ERROR):
                    print("Ocurrió un error en el servidor.\n")
                    break

            self.cliente.set_estado(ClienteEstado.S2)

        except socket.error as error:
            if (error.errno == 2):
                err_msg  = "Error: El archivo que solicitaste subir no existe."
                err_msg += " ¿Pusiste la ubicación correcta?\n"
                print(err_msg)

                self.cliente.set_estado(ClienteEstado.S2)

            elif (error.errno == 9):
                print("Error: Se perdió la comunicación con el servidor.")
                self.cliente.set_estado(ClienteEstado.LOSS_CONN)

            else:
                print(str(error))
                self.cliente.set_estado(ClienteEstado.S2)

    def visualizar_archivos(self):
        """
        Le manda mensaje al servidor para que le muestre los archivos
        que se encuentran en el servidor.
        """
        try:
            print("Solicitando archivos al servidor...")
            self.cliente.set_estado(ClienteEstado.S5)
            self.cliente.solicitud_para_ver_archivos()

            while (self.cliente.is_connected):
                _estado = self.cliente.get_estado()

                if (_estado == ClienteEstado.S6):
                    return

                elif (_estado == ClienteEstado.S2):
                    print("No tienes archivos almacenados en el servidor.\n")
                    return

        except socket.error as error:
            if (error.errno == 9):
                print("Error: Se perdió la comunicación con el servidor.")
                self.cliente.set_estado(ClienteEstado.LOSS_CONN)                

    def mostrar_acciones_archivos(self):
        """
        Le muestra al cliente las acciones que puede
        realizar después de ver la lista de archivos.
        """
        s  = "Introduzca el número de la acción que "
        s += "quiere realizar. \n"
        s += "  1) Descargar un archivo. \n"
        s += "  2) Eliminar un archivo. \n"
        s += "  3) Regresar al menú principal \n"

        print(s)

        accion = input("Acción: ")

        if (accion == "1"):
            self.cliente.set_estado(ClienteEstado.S7)

        elif (accion == "2"):
            self.cliente.set_estado(ClienteEstado.S12)

        elif (accion == "3"):
            self.cliente.set_estado(ClienteEstado.SR)

        else:
            self.cliente.set_estado(ClienteEstado.SR)
            print("Acción no reconocida.\n")

    def menu_principal(self):
        """
        Pasa del estado en el que se encuentre al
        estado inicial tras el inicio de sesión
        """
        try:
            self.cliente.abortar_mision()
            self.cliente.set_estado(ClienteEstado.S2)

        except socket.error as error:
            if (error.errno == 9):
                print("Error: Se perdió la comunicación con el servidor.")
                self.cliente.set_estado(ClienteEstado.LOSS_CONN)

    def visualizar_archivo(self):
        """
        Le manda mensaje al servidor para que le regrese el
        archivo que desea visualizar.
        """
        try:
            print("Ingresa el identificador del archivo que quieres ver")

            id_file = input("ID: ")

            print("Solicitando el archivo " + id_file + "...")
            self.cliente.set_estado(ClienteEstado.S7)
            self.cliente.solicitud_para_ver_archivo(id_file)

            while (self.cliente.is_connected):
                _estado = self.cliente.get_estado()

                if (_estado == ClienteEstado.S8_0):
                    print("El archivo se ha guardado exitosamente.")
                    break

                elif (_estado == ClienteEstado.S8_1):
                    print("Error: Ocurrió un error al guardar el archivo.")
                    break

            self.cliente.set_estado(ClienteEstado.S2)

        except ValueError as e:
            err_msg  = "Error: Asegúrate de que el identificador introducido "
            err_msg += "sea un número..\n"
            print(err_msg)
            self.cliente.set_estado(ClienteEstado.SR)
        
        except socket.error as error:
            if (error.errno == 9):
                print("Error: Se perdió la comunicación con el servidor.")
                self.cliente.set_estado(ClienteEstado.LOSS_CONN)

    def solicitar_eliminar_archivo(self):
        """
        Le pregunta al cliente si esta seguro de querer
        eliminar el archivo y si es así entonces manda la
        solicitud.
        """
        try:
            print("Introduce el identificador del archivo que quieres eliminar")
            id_file = input("ID: ")

            print("¿Estas seguro de querer eliminar el archivo " + id_file + "?")

            while (self.cliente.is_connected):
                ans = input("Respuesta[S/N]: ")
                if (ans == "S"):
                    print("Solicitando que se elimine el archivo " + id_file + "...")
                    self.cliente.set_estado(ClienteEstado.ESPERANDO)
                    self.cliente.solicitud_para_eliminar_archivo(id_file)
                    break

                elif (ans == "N"):
                    self.cliente.set_estado(ClienteEstado.S2)
                    return

                else:
                    print("Por favor introduzca S o N")

            while (self.cliente.is_connected):
                _estado = self.cliente.get_estado()

                if (_estado == ClienteEstado.S12_0):
                    print("El archivo se ha eliminado exitosamente.")
                    break

                elif (_estado == ClienteEstado.S12_1):
                    print("Error: Ocurrió un error al eliminar el archivo.")
                    break

                elif (_estado == ClienteEstado.S2):
                    print("Error: El comando solicitado no fue reconocido.")
                    return
            
            if (self.cliente.is_connected):
                self.cliente.set_estado(ClienteEstado.S2)
            else:
                self.cliente.set_estado(ClienteEstado.LOSS_CONN)

        except ValueError as e:
            err_msg  = "Error: Asegúrate de que el identificador introducido "
            err_msg += "sea un número..\n"
            print(err_msg)
            self.cliente.set_estado(ClienteEstado.SR)

        except socket.error as error:
            if (error.errno == 9):
                print("Error: Se perdió la comunicación con el servidor.")
                self.cliente.set_estado(ClienteEstado.LOSS_CONN)      

    def cambiar_imagen(self):
        """
        Le indica al servidor que el cliente quiere cambiar de imagen
        de perfil.
        """
        try:
            msg = "Introduzca la ubicación de la nueva imagen de perfil: "
            file_path = input(msg)

            msg = "Introduzca la extensión del archivo (gif, jpg o png): "
            while (self.cliente.is_connected):
                name = input(msg)

                if (name in ["gif", "jpg", "png"]):
                    break
                else:
                    print("La extensión sólo puede ser gif, jpg o png")
                    self.cliente.set_estado(ClienteEstado.S2)
                    return

            print("\nSolicitando que se actualice la imagen de perfil...")
            self.cliente.set_estado(ClienteEstado.ESPERANDO)
            self.cliente.cambiar_foto_perfil(name, file_path)

            while (self.cliente.is_connected):
                _estado = self.cliente.get_estado()

                if (_estado == ClienteEstado.S11_0):
                    print("Se ha actualizado su imagen de perfil.\n")
                    break

                elif (_estado == ClienteEstado.S11_1):
                    print("Error: Ha ocurrido un error al actualizar su imagen.\n")
                    break

                elif (_estado == ClienteEstado.S2):
                    print("Error: El comando solicitado no fue reconocido.")
                    return

            self.cliente.set_estado(ClienteEstado.S2)

        except socket.error as error:
            if (error.errno == 2):
                err_msg  = "Error: El archivo que solicitaste subir no existe."
                err_msg += " ¿Pusiste la ubicación correcta?\n"
                print(err_msg)
                self.cliente.set_estado(ClienteEstado.S2)

            elif (error.errno == 9):
                print("Error: Se perdió la comunicación con el servidor.")
                self.cliente.set_estado(ClienteEstado.LOSS_CONN)      

            else:
                print(str(error))
                self.cliente.set_estado(ClienteEstado.S2)

    def eliminar_cuenta(self):
        """
        Le indica al servidor que el cliente quiere eliminar
        su cuenta.
        """
        try:
            msg  = "¿Estas seguro de querer eliminar tu cuenta?"
            msg += " Todos los archivos que subiste al servidor "
            msg += "se eliminaran permanentemente."
            print(msg)

            while (self.cliente.is_connected):
                ans = input("Respuesta[S/N]: ")
                if (ans == "S"):
                    print("Eliminando tu cuenta...")
                    self.cliente.set_estado(ClienteEstado.S10)
                    self.cliente.solicitud_para_eliminar_cuenta()
                    break

                elif (ans == "N"):
                    self.cliente.set_estado(ClienteEstado.S2)
                    return

                else:
                    print("Por favor introduzca S o N")

            if (not self.cliente.is_connected):
                self.cliente.set_estado(ClienteEstado.LOSS_CONN)

        except socket.error as error:
            if (error.errno == 9):
                print("Error: Se perdió la comunicación con el servidor.")
                self.cliente.set_estado(ClienteEstado.LOSS_CONN)
        

    def cerrar_sesion(self):
        """
        Cierra la sesión con el servidor.
        """
        try:
            print("\nCerrando la conexión con el servidor...")
            self.cliente.solicitud_para_desconectarse()
            print("Conexión cerrada con éxito.\n")
            self.cliente.set_estado(ClienteEstado.S10)

        except socket.error as error:
            if (error.errno == 9):
                print("Error: Se perdió la comunicación con el servidor.")
                self.cliente.set_estado(ClienteEstado.LOSS_CONN)      

    def cerrar(self):
        """
        Cierra la aplicación.
        """
        print("Gracias por utilizar McCloud.")

    def transiciones(self):
        """
        El cliente toma una acción en especifico dependiendo
        del estado en el que se encuentre actualmente.
        """
        while True:
            _estado = self.cliente.get_estado()

            if (_estado == ClienteEstado.INIT):
                self.bienvenida()

            elif (_estado == ClienteEstado.CONN):
                self.intentar_conexion()

            elif (_estado == ClienteEstado.S0):
                self.inicia_o_registra()

            elif (_estado == ClienteEstado.SA):
                self.registrarse()

            elif (_estado == ClienteEstado.S1):
                self.iniciar_sesion()

            elif (_estado == ClienteEstado.S2):
                self.mostrar_acciones()

            elif (_estado == ClienteEstado.S3):
                self.solicitar_subida()

            elif (_estado == ClienteEstado.S4):
                self.subir_archivo()

            elif (_estado == ClienteEstado.S5):
                self.visualizar_archivos()

            elif (_estado == ClienteEstado.S6):
                self.mostrar_acciones_archivos()

            elif (_estado == ClienteEstado.S7):
                self.visualizar_archivo()

            elif (_estado == ClienteEstado.S9):
                self.cerrar_sesion()

            elif (_estado == ClienteEstado.S10):
                self.cerrar()
                break

            elif (_estado == ClienteEstado.S11):
                self.cambiar_imagen()

            elif (_estado == ClienteEstado.S12):
                self.solicitar_eliminar_archivo()

            elif (_estado == ClienteEstado.S13):
                self.eliminar_cuenta()

            elif (_estado == ClienteEstado.SR):
                self.menu_principal()

            elif (_estado == ClienteEstado.LOSS_CONN):
                self.perdio_conexion()
                break

            else:
                print("FATAL: El cliente se ha vuelto inestable.")
                print(_estado)
                break


    def imprime_archivos(self, data):
        """
        Imprime la lista de archivos que recibio del
        servidor.
        :param data: Lista de tuplas que contiene  ID y nombre de los archivos.
        """
        files        = []
        longest_name = 0
        longest_ID   = 0

        for d in data:
            ID   = int.from_bytes(d[0:4], 'big')
            name = d[4:].decode('utf-8')

            files.append((ID, name))

            if (len(name) > longest_name):
                longest_name = len(name)
            if (len(str(ID)) > longest_ID):
                longest_ID   = len(str(ID))

        row_format_header_ID   = "{:^" + str(longest_ID) + "}"
        row_format_header_name = "{:^" + str(longest_name) + "}"

        row_format_ID   = "{:>" + str(longest_ID + 2) + "}"
        row_format_name = "{:<" + str(longest_name + 5) + "}"

        row_format_header = row_format_header_ID + " " + row_format_header_name
        row_format        = row_format_ID + " " + row_format_name

        row_lines_ID   = "-" * (longest_ID + 2)
        row_lines_name = "-" * longest_name

        print(row_format_header.format("ID", "Nombre"))
        print(row_format.format(row_lines_ID, row_lines_name))
        for ID, name in files:
            print(row_format.format(ID, name))

        print()

    def crea_archivo(self, size):
        """
        Guarda los contenidos recibidos del servidor en un
        archivo con nombre especificado por el cliente.
        :param size: El tamaño del archivo.
        """
        name = "test.jpg"

        self.cliente.crear_archivo(name, size)
        self.cliente.set_estado(ClienteEstado.S7_0)

    def crea_imagen_perfil(self, size):
        """
        Guarda los contenidos recibidos del servidor en un 
        archivo asignado para imagen de perfil del cliente.
        :param size: El tamaño del archivo.
        """
        name = "perfil"
        self.cliente.crear_archivo(name, size)
        self.cliente.set_estado(ClienteEstado.S1_0)

    def construye_archivo(self, contents):
        """
        Construye el archivo dado los contenidos que se reciben.
        :param contents: Los contenidos del archivo.
        """
        completo = self.cliente.extender_archivo(contents)
        _estado  = self.cliente.get_estado()
        
        if (completo >= 0):
            if (_estado == ClienteEstado.S1_0):
                self.cliente.set_estado(ClienteEstado.S2)
                self.cliente.guardar_archivo("perfil")

            elif (_estado == ClienteEstado.S7_0):
                s  = "Ingrese el nombre del archivo con el que "
                s += "quiere guardar los contenidos del servidor."
                print(s)

                name = input("Nombre: ")
                self.cliente.guardar_archivo(name)

                s  = "¿Quieres visualizar el archivo?"
                print(s)
                
                ans = input("Respuesta[S/N]: ")

                if (ans == "S"):
                    self.mostrar_imagen(name)
            
                if (completo == 2):
                    code         = list(contents)[-2]
                    confirmation = list(contents)[-1]

                    if (confirmation == 0) and (code == 45):
                        self.cliente.set_estado(ClienteEstado.S8_0)
                    elif (confirmation == 1) or (code != 45):
                        self.cliente.set_estado(ClienteEstado.S8_1)

                elif (completo == 0):
                    self.cliente.set_estado(ClienteEstado.S7_1)

    def mostrar_imagen(self, path):
        """
        Manda a llamar un comando para mostar la imagen
        pasada como argumento.
        """
        subprocess.run([self.image_viewer, path])

    def perdio_conexion(self):
        """
        Se perdió la conexión con el servidor.
        """
        print("\nSe perdió la conexión con el servidor.")
        print("Cerrando el programa...")
        print("\nGracias por utilizar McCloud.")
        self.cliente.set_estado(ClienteEstado.LOSS_CONN)

    def mensaje_del_servidor(self, msg):
        """
        Cambia el estado del cliente dependiendo de lo que
        el servidor responda.
        """
        if (msg == -1):
            self.cliente.set_estado(ClienteEstado.LOSS_CONN)
            return

        _estado = self.cliente.get_estado()

        # El ciente esta recibiendo los contenidos de un archivo.
        if (_estado == ClienteEstado.S7_0) or (_estado == ClienteEstado.S1_0):
            self.construye_archivo(msg)
            return

        byte_list = list(msg)

        code = byte_list[0]

        # El cliente se regristró con éxito.
        if (code == 1):
            self.cliente.set_estado(ClienteEstado.EXITO)

        # El cliente inició sesión con éxito.
        elif (code == 20):
            i = 0
            for b in byte_list:
                if (b == 255):
                    name     = msg[1:i].decode('utf-8')
                    img_size = int.from_bytes(msg[i+1:i+5], 'big')
                    contents = msg[i+5:]
                    break

                i = i + 1

            self.cliente.set_name(name)
            self.crea_imagen_perfil(img_size)
            self.construye_archivo(contents)

        # Sí hay espacio en el servidor para almacenar el archivo.
        elif (code == 32):
            self.cliente.set_estado(ClienteEstado.S4)

        # Si el archivo almacenó con exito o no en el servidor.
        elif (code == 33):
            if (byte_list[1] == 0):
                self.cliente.set_estado(ClienteEstado.EXITO)
            else:
                self.cliente.set_estado(ClienteEstado.ERROR)

        # No hay espacio en el servidor para almacenar el archivo.
        elif (code == 34):
            self.cliente.set_estado(ClienteEstado.S2)

        # El cliente recibe la lista de archivos que tiene en el servidor.
        elif (code == 42):
            data  = []
            files = []

            n = len(byte_list)
            i = 1

            # Obtiene cada archivo por medio de los delimitadores.
            for j in range(1, n):
                #Encontró el delimitador
                if (byte_list[j] == 255):
                    data.append(msg[i:j])
                    i = j + 1

            if (len(data) == 0):
                self.cliente.set_estado(ClienteEstado.S2)
            else:
                self.imprime_archivos(data)
                self.cliente.set_estado(ClienteEstado.S6)

        # Recibe el archivo solicitado.
        elif (code == 44):
            size     = int.from_bytes(msg[1:5], 'big')
            contents = msg[5:]

            self.crea_archivo(size)
            self.construye_archivo(contents)

        # Termina de recibir el archivo solicitado.
        elif (code == 45):
            if (byte_list[1] == 0):
                self.cliente.set_estado(ClienteEstado.S8_0)
            else:
                self.cliente.set_estado(ClienteEstado.S8_1)

        # Respuesta a la solicitud de eliminar un archivo.
        elif (code == 47):
            if (byte_list[1] == 0):
                self.cliente.set_estado(ClienteEstado.S12_0)
            else:
                self.cliente.set_estado(ClienteEstado.S12_1)

        # Respuesta a la solicitud de actualizar imagen de perfil..
        elif (code == 52):
            if (byte_list[1] == 0):
                self.cliente.set_estado(ClienteEstado.S11_0)
            else:
                self.cliente.set_estado(ClienteEstado.S11_1)

        # Comando no reconocido.
        elif (code == 90) or (code == 81):
            self.cliente.set_estado(ClienteEstado.S2)

        # Falló el inicio de sesión o registro.
        elif (code == 91):
            self.cliente.set_estado(ClienteEstado.ERROR)

        else:
            print("Mensaje no reconocido del servidor:")
            print(str(msg))
            print(str(byte_list))
