import os

from socket import socket
from socket import error as SocketError

from src.ClienteEstado import *
from src.Archivo import *

"""
Una clase para el cliente.

Para generar la documentación en HTML de este
módulo, ejecutar el siguiente comando:

    pydoc -w src.Cliente
"""

class Cliente(object):
    """
    Un cliente se conecta a un servidor y es capaz
    de hacer las siguientes acciones:

    1) Iniciar sesión en un servidor mandándole sus
       credenciales: nombre de usuario y contraseña.
    2) Enviar una solicitud para la subida de un
       nuevo archivo.
    3) Envío del archivo a almacenar.
    4) Solicitud para ver los archivos que hay en el
       servidor.
    5) Solicitud para ver un archivo.
    6) Solicitud para cerrar la conexión con el servidor.

    Encapsula los siguientes atributos:

    * buffer_size
        El tamaño del buffer.
    * socket
        El socket que el cliente usará para la comunicación.
    * user_name
        El nombre de usuario del cliente.
    * is_connected
        Indica si el cliente se encuentra conectado a un servidor.
    * host_address
        La dirección del servidor.
    * host_port
        El puerto del servidor.
    * listener
        El función escucha del cliente
    * status
        El estado del cliente

    """

    buffer_size  = 1024
    socket       = None
    user_name    = None
    is_connected = False
    host_address = None
    host_port    = None
    listener     = None
    status       = None

    def __init__(self):
        """
        Construye un cliente con sus atributos por omisión.
        """

        self.socket    = socket()
        self.user_name = "Desconocido"

        self.socket.settimeout(5)

    def set_name(self, user_name):
        """
        Guarda el nombre de usuario del cliente.
        :param user_name: El nombre de usuario del cliente.
        """
        self.user_name = user_name

    def get_name(self):
        """
        Regresa el nombre de usuario del cliente.
        :return: El nombre de usuario del cliente.
        """
        return self.user_name

    def set_estado(self, _status):
        """
        Guarda el estado del cliente.
        :param _status: El estado del cliente.
        """
        self.status = _status

    def get_estado(self):
        """
        Regresa el estado actual del cliente.
        :return: El estado actual del cliente.
        """
        return self.status

    def set_address(self, host_address):
        """
        Guarda la dirección del servidor.
        :param host_address: La dirección del servidor.
        """
        self.host_address = host_address

    def set_port(self, host_port):
        """
        Guarda el puerto del servidor.
        :param host_port: El puerto del servidor.
        """
        try:
            self.host_port = int(host_port)
        except (ValueError, OverflowError) as error:
            raise error

    def set_escucha(self, f):
        """
        Indica cuál es la función escucha del cliente.
        :param f: La función escucha.
        """
        self.listener = f

    def conectar(self):
        """
        Trata de conectarse al servidor.
        En caso de error regresa una excepción.
        """
        try:
            self.socket.connect((self.host_address, self.host_port))
            self.is_connected = True
            self.socket.settimeout(None)
        except Exception as error:
            raise error

    def desconectar(self):
        """
        Trata de cerrar la conexión al servidor.
        En caso de error regresa una excepción.
        """
        try:
            self.socket.close()
            self.is_connected = False
        except Exception as error:
            raise error

    def registrar(self, user_name, password):
        """
        Envía los datos para crear un nuevo
        registro en el servidor.
        En caso de error regresa una excepción.
        :param user_name: El nombre de usuario del cliente.
        :param password:  La constraseña del cliente.
        """
        try:
            msg  = bytearray(bytes([0]))
            name = bytes(user_name, 'utf-8')
            pw   = bytes(password, 'utf-8')

            msg.extend(name)
            msg.extend(bytearray(bytes([255]))) #delimitador
            msg.extend(pw)

            self.socket.send(msg)

        except Exception as error:
            raise error

    def inicia_sesion(self, user_name, password):
        """
        Envía las credenciales para iniciar sesión
        al servidor.
        En caso de error regresa una excepción.
        :param user_name: El nombre de usuario del cliente.
        :param password:  La contraseña del cliente.
        """
        try:
            msg  = bytearray(bytes([10]))
            name = bytes(user_name, 'utf-8')
            pw   = bytes(password, 'utf-8')

            msg.extend(name)
            msg.extend(bytearray(bytes([255]))) #delimitador
            msg.extend(pw)

            self.socket.send(msg)

        except Exception as error:
            raise error



    def solicitud_para_archivo(self, file_path):
        """
        Envía una solicitud para subir un archivo
        al servidor.
        En caso de error regresa una excepción.
        """
        try:
            msg       = bytearray(bytes([31]))
            file_size = os.path.getsize(file_path)

            l_bytes = file_size.to_bytes(4, 'big')

            msg.extend(l_bytes)

            self.socket.send(msg)

        except Exception as error:
            raise error

    def enviar_archivo(self, name, file_path):
        """
        Envía un archivo al servido para que sea
        almacenado.
        En caso de error regresa una excepción.
        :param file_path: La ubicación del archivo a subir.
        """
        try:
            msg       = bytearray(bytes([32]))
            name      = bytes(name, 'utf-8')
            f         = open(file_path, 'rb')
            file_size = os.path.getsize(file_path)
            l_bytes   = file_size.to_bytes(4, 'big')

            msg.extend(name)
            msg.extend(bytearray(bytes([255]))) #delimitador
            msg.extend(l_bytes)

            for byte_line in f.readlines():
                msg.extend(byte_line)

            self.socket.send(msg)

        except Exception as error:
            raise error

    def solicitud_para_ver_archivos(self):
        """
        Envía una solicitud para ver los archivos que hay
        en el servidor.
        En caso de error regresa una excepción.
        """
        try:
            msg = bytearray(bytes([41]))

            self.socket.send(msg)

        except Exception as error:
            raise error


    def solicitud_para_ver_archivo(self, id_file):
        """
        Envía una solicitud para ver el archivo con el
        identificador dado que se encuentra en el
        servidor.
        En caso de error regresa una excepción.
        :param id_file: El identificador del archivo a ver.
        """
        try:
            msg = bytearray(bytes([43]))
            ID  = int(id_file).to_bytes(4, 'big')

            msg.extend(ID)

            self.socket.send(msg)

        except Exception as error:
            raise error

    def crear_archivo(self, name, size):
        """
        Crea un archivo de tamaño y con el nombre que se 
        pasó.
        :param name: El nombre del archivo.
        :param size: El tamaño del archivo.
        """
        self.archivo = Archivo(name, size)

    def extender_archivo(self, contents):
        """
        Escribe los contenidos recibidos en el archivo 
        que actualmente tiene el cliente.
        :param contents: Los contenidos a agregar del archivo.
        :return: True si el archivo ya esta completo, False en otro caso.
        """
        self.archivo.extender(contents)
        return self.archivo.esta_completo()

    def guardar_archivo(self, name):
        """
        Guarda los contenidos del archivo con el nombre
        que se le pasó.
        :param name: El nombre del archivo.
        """
        self.archivo.set_name(name)
        self.archivo.guardar()

    def solicitud_para_eliminar_archivo(self, id_file):
        """
        Envía una solicitud para eliminar el archivo con el
        identificador dado que se encuentra en el
        servidor.
        En caso de error regresa una excepción.
        :param id_file: El identificador del archivo a ver.
        """
        try:
            msg = bytearray(bytes([46]))
            ID  = int(id_file).to_bytes(4, 'big')

            msg.extend(ID)

            self.socket.send(msg)

        except Exception as error:
            raise error

    def cambiar_foto_perfil(self, name, file_path):
        """
        Envía una solicitud para cambiar la imagen de
        perfil que se encuentra en el servidor.
        :param file_path: La ubicación de la imagen de perfil.
        """
        try:
            # Envía la nueva foto de perfil al servidor.
            msg       = bytearray(bytes([51]))
            name      = bytes(name, 'utf-8')
            f         = open(file_path, 'rb')
            file_size = os.path.getsize(file_path)
            l_bytes   = file_size.to_bytes(4, 'big')

            local     = open('perfil', 'wb')

            msg.extend(name)
            msg.extend(bytearray(bytes([255]))) #delimitador
            msg.extend(l_bytes)

            for byte_line in f.readlines():
                # Construye la imagen para el mensaje que se enviará
                # al servidor.
                msg.extend(byte_line)
                
                # Hace el cambio local de la imagen de perfil del 
                # cliente.
                local.write(byte_line)

            self.socket.send(msg)
            

        except Exception as error:
            raise error

    def solicitud_para_eliminar_cuenta(self):
        """
        Envia una solicitud para eliminar la cuenta
        actual del cliente.
        """
        try:
            msg = bytearray(bytes([60]))

            self.socket.send(msg)

        except Exception as error:
            raise error

    def solicitud_para_desconectarse(self):
        """
        Envía una solicitud para desconectarse del servidor.
        En caso de error regresa una excepción.
        """
        try:
            msg = bytearray(bytes([100]))

            self.socket.send(msg)

            self.desconectar()

        except Exception as error:
            raise error

    def escucha_servidor(self):
        """
        Mientras el mensaje sea distinto del vacío y se encuentre
        conectado el cliente, este va a esperar por un mensaje
        del servidor. Al recibir un mensaje del servidor lo
        va a mandar por el escucha.
        """
        msg = "..."

        while True:
            try:
                if (self.is_connected):
                    msg = self.socket.recv(self.buffer_size)

                    if (msg):
                        self.listener(msg)
                    else:
                        break

            except Exception as error:
                raise error

        if (self.is_connected):
            self.listener(-1)
            self.desconectar()

    def abortar_mision(self):
        """
        Envía una solicitud al servidor para que regrese
        al estado tras iniciar sesión.
        En caso de error regresa una excepción.
        """
        try:
            msg = bytearray(bytes([80]))

            self.socket.send(msg)

        except Exception as error:
            raise error
