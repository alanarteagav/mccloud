from enum import Enum

"""
Una enumeración para los diferentes estados en los que
se puede encontrar el cliente.

Para generar la documentación en HTML de este
módulo, ejecutar el siguiente comando:

    pydoc -w src.CLI
"""
class ClienteEstado(Enum):
    """
    ClienteEstado es una enumeración que representa
    los diferentes estados en los que se puede
    encontrar un cliente.
    """

    # Estado inicial de la aplicacion.
    INIT = -3

    # El cliente busca conectarse al servidor
    CONN = -2

    # Estado inicial del cliente.
    S0 = 0

    # El cliente busca registrarse.
    SA = -1

    # El cliente busca iniciar sesión.
    S1 = 1

    # El cliente esta recibiendo la imagen de perfil
    S1_0 = 1.1 # La imagen de perfil se sigue recibiendo.

    # El cliente inició sesión.
    S2 = 2

    # El cliente solicita subir un archivo.
    S3 = 3

    # El cliente sube un archivo.
    S4 = 4

    # El cliente solicita ver sus archivos.
    S5 = 5

    # El cliente recibe una lista de sus archivos.
    S6 = 6

    # El cliente solicita ver un archivo en especifico.
    S7 = 7

    # El cliente esta recibiendo el archivo que solicitó.
    S7_0 = 7.1 # El archivo se sigue recibiendo.

    # El cliente termina de recibir el archivo solicitado.
    S8_0 = 8.0 # El archivo se recibió con éxito.
    S8_1 = 8.1 # Ocurrió un error al recibir el archivo.

    # El cliente busca cerrar la sesión..
    S9 = 9

    # El ciente cierra la aplicación.
    S10 = 10

    # El cliente solicita cambiar su foto de perfil.
    S11 = 11

    # El cliente recibe una respuesta sobre el cambio de foto.
    S11_0 = 11.1 # La foto se actualizó con éxito.
    S11_1 = 11.2 # Ocurrió un error al actualizar la foto de perfil.

    # El cliente solicita eliminar un archivo.
    S12 = 12

    # El cliente recibe respuesta sobre el archivo eliminado
    S12_0 = 12.1 # El archivo fue eliminado con éxito.
    S12_1 = 12.2 # Ocurrió un error al eliminar el archivo.


    # El cliente solicita eliminar su cuenta.
    S13 = 13

    # El cliente solicita regresar al estado inicial
    SR = 66

    ERROR     = 22
    ESPERANDO = 21
    EXITO     = 20

    LOSS_CONN = 999
