# script bash para configurar el sistema

# actualización de snap
sudo apt update
sudo apt install snapd
sudo snap install core

# instalación de go
sudo snap install go --classic

# instalación de postgres
sudo apt install postgresql postgresql-contrib

# ajuste en configuración de postgres
sudo cp resources/sql/pg_hba.conf /etc/postgresql/12/main/pg_hba.conf

# reinicio del servicio postgres
sudo service postgresql restart

# creación de la base de datos
psql -U postgres -f resources/sql/mccloud.sql

# ejecución del script para instalar el servidor
. servidor/buildServidor.sh
